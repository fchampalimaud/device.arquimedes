## What file .gitignore should I use?

When you clone, fork or branch this repository, you are encouraged to create a file named **.gitignore** on your local repository root to ensure that you are not sending unnecessary files to the repository. For this repository, it is recommended a **.gitignore** file with the following content:

    desktop.ini
    .gitignore
    **/Downloads
    **/downloads
    **/Debug
    **/debug
    *.atsuo
    *.o
    *.d