/*
 * Arquimedes.c
 *
 * Created: 05/04/2017 13:41:50
 *  Author: Filipe Carvalho
 */ 

#include "cpu.h"
#include "uart0.h"

#include <avr/io.h>
#define F_CPU 4000000
#include <util/delay.h>

// byte 0: [0] [IN03] [IN02]  [IN01] [ADC4] [ADC3] [ADC2] [ADC1]
// byte 1: [1] [x]    [ADC10] [ADC9] [ADC8] [ADC7] [ADC6] [ADC5]
uint8_t byte0;
uint8_t byte1;

uint16_t adc_ffset;

void initialize_ios (void)
{
	io_pin2in(&PORTC, 7, PULL_IO_TRISTATE, SENSE_IO_EDGES_BOTH);	// STATE_CTRL
	io_set_int(&PORTC, INT_LEVEL_MED, 1, (1<<7), false);			// STATE_CTRL
	
	io_pin2out(&PORTR, 0, OUT_IO_DIGITAL, false);					// STATE
	set_io(PORTR, 0);												// STATE = 1
	
	io_pin2in(&PORTC, 2, PULL_IO_DOWN, SENSE_IO_EDGES_BOTH);		// IN01
	io_pin2in(&PORTD, 0, PULL_IO_DOWN, SENSE_IO_EDGES_BOTH);		// IN02
	io_pin2in(&PORTA, 3, PULL_IO_DOWN, SENSE_IO_EDGES_BOTH);		// IN03
    
    io_pin2in(&PORTC, 4, PULL_IO_DOWN, SENSE_IO_EDGES_BOTH);		// UCUC
    io_set_int(&PORTC, INT_LEVEL_LOW, 0, (1<<4), false);			// UCUC
	
	io_pin2out(&PORTA, 2, OUT_IO_DIGITAL, false);					// OUT01
	io_pin2out(&PORTA, 1, OUT_IO_DIGITAL, false);					// OUT02
	io_pin2out(&PORTA, 0, OUT_IO_DIGITAL, false);					// OUT03
	io_pin2out(&PORTD, 2, OUT_IO_DIGITAL, false);					// OUT04
	io_pin2out(&PORTD, 1, OUT_IO_DIGITAL, false);					// OUT05
	
	/* Initialize ADC */	
	PR_PRPA &= ~(PR_ADC_bm);									// Remove power reduction
	ADCA_CTRLA = ADC_ENABLE_bm;									// Enable ADCA
	ADCA_CTRLB = ADC_CURRLIMIT_HIGH_gc;							// High current limit, max. sampling rate 0.5MSPS
	ADCA_CTRLB  |= ADC_RESOLUTION_12BIT_gc;						// 12-bit result, right adjusted
	ADCA_REFCTRL = ADC_REFSEL_INTVCC_gc;						// VCC/1.6 = 3.3/1.6 = 2.0625 V
	ADCA_PRESCALER = ADC_PRESCALER_DIV128_gc;					// 250 ksps
																// Propagation Delay = (1 + 12[bits]/2 + 1[gain]) / fADC[125k] = 32 us
																// Note: For single measurements, Propagation Delay is equal to Conversion Time
		
	ADCA_CH0_CTRL = ADC_CH_INPUTMODE_SINGLEENDED_gc;	        // Single-ended positive input signal
	ADCA_CH0_MUXCTRL = 4 << 3;									// Pin 4 for zero calibration
	ADCA_CH0_INTCTRL = ADC_CH_INTMODE_COMPLETE_gc;		        // Rise interrupt flag when conversion is complete
	ADCA_CH0_INTCTRL |= ADC_CH_INTLVL_OFF_gc;				    // Interrupt is not used
		
	/* Wait 10 us to stabilization before measure the zero/GND value */
	timer_type0_enable(&TCD0, TIMER_PRESCALER_DIV2, 80, INT_LEVEL_OFF);
	while(!timer_type0_get_flag(&TCD0));
	timer_type0_stop(&TCD0);
		
	/* Measure and safe adc offset */
	ADCA_CH0_CTRL |= ADC_CH_START_bm;						    // Start conversion
	while(!(ADCA_CH0_INTFLAGS & ADC_CH_CHIF_bm));		        // Wait for conversion to finish
	ADCA_CH0_INTFLAGS = ADC_CH_CHIF_bm;						    // Clear interrupt bit 
	adc_ffset = ADCA_CH0_RES;									// Read offset

	/* Point ADC to the right channel */
	ADCA_CH0_MUXCTRL = 5 << 3;									// Select pin 4 for further conversions
	ADCA_CH0_CTRL |= ADC_CH_START_bm;						    // Force the first conversion	
	while(!(ADCA_CH0_INTFLAGS & ADC_CH_CHIF_bm));		        // Wait for conversion to finish
	ADCA_CH0_INTFLAGS = ADC_CH_CHIF_bm;						    // Clear interrupt bit
}

int main(void)
{   
	cpu_config_clock(4000000, true, true);
    
	initialize_ios();
    
    /* 250 Kbits */
    uart0_init(0, 0, false);
    uart0_enable();
    
	PMIC_CTRL = PMIC_CTRL | PMIC_RREN_bm | PMIC_LOLVLEN_bm | PMIC_MEDLVLEN_bm;
    __asm volatile("sei");
		
	while(1) {}
}


void read_dig_inputs (void)
{
    byte0 = (byte0 & 0x0F) | (PORTC_IN & (1<<2) ? 0x10 : 0) | (PORTD_IN & (1<<0) ? 0x20 : 0) | (PORTA_IN & (1<<3) ? 0x40 : 0);
}

void read_ana_input (void)
{
    ADCA_CH0_CTRL |= ADC_CH_START_bm;					// Force the conversion
    while(!(ADCA_CH0_INTFLAGS & ADC_CH_CHIF_bm));		// Wait for conversion to finish
    ADCA_CH0_INTFLAGS = ADC_CH_CHIF_bm;					// Clear interrupt bit

    uint16_t adc = ((ADCA_CH0_RES & 0x0FFF) - adc_ffset) >> 1;
    
    if (adc > 2048)
        adc = 0;

    // byte 0: [0] [IN03]  [IN02]  [IN01] [ADC4] [ADC3] [ADC2] [ADC1]
    // byte 1: [1] [ADC11] [ADC10] [ADC9] [ADC8] [ADC7] [ADC6] [ADC5]

    byte0 = (byte0 & 0x70) | ((uint8_t)(adc & 0x0F));
    byte1 = 0x80 | ((uint8_t)(adc >> 4));
}

ISR(PORTC_INT1_vect)
{
	if (PORTC_IN & (1<<7))
	{
		set_io(PORTR, 0);
	}
	else
	{
		clear_io(PORTR, 0);
	}
}

ISR(PORTC_INT0_vect, ISR_NAKED)
{
    read_dig_inputs();
    read_ana_input();
    
    _delay_us(70-35);   // 70 was fine (removing 35 us to have completely sure)
    
    uart0_xmit_now_byte(byte0);
    _delay_us(64+10);   // 64 was fine (adding more 10 us to have more time between bytes
    uart0_xmit_now_byte(byte1);
    
    reti();
}

void uart0_rcv_byte_callback(uint8_t byte)
{
    if (byte & (1<<1)) PORTA_OUTSET = (1<<2); else PORTA_OUTCLR = (1<<2);	// OUT01
    if (byte & (1<<2)) PORTA_OUTSET = (1<<1); else PORTA_OUTCLR = (1<<1);	// OUT02
    if (byte & (1<<3)) PORTA_OUTSET = (1<<0); else PORTA_OUTCLR = (1<<0);	// OUT03
    if (byte & (1<<4)) PORTD_OUTSET = (1<<2); else PORTD_OUTCLR = (1<<2);	// OUT04
    if (byte & (1<<5)) PORTD_OUTSET = (1<<1); else PORTD_OUTCLR = (1<<1);	// OUT05
}
