#include "cpu.h"
#include "hwbp_core_types.h"
#include "app_ios_and_regs.h"
#include "app_funcs.h"
#include "hwbp_core.h"

extern AppRegs app_regs;

#define STEP_PERIOD_MILLISECONDS 4
static uint8_t step_period_counter = 0;

static uint8_t send_event_counter = 0;

extern uint16_t reset_motor_traveling;
static bool send_position_event_counter = 0;

void load_motor(void)
{
    if (++step_period_counter != STEP_PERIOD_MILLISECONDS)
    {
        clr_M_STEP;
        return;
    }
    
    step_period_counter = 0;    
    
    if (app_regs.REG_RESET_MOTOR == B_RST_MOTOR)
    {
        set_M_DIR;
        
        if (reset_motor_traveling)
        {
            set_M_STEP;
            
            if (--reset_motor_traveling == 0)
            {
                app_regs.REG_RESET_MOTOR = 0;
            }
        }
    }
    else
    {
        if (app_regs.REG_POS_CURRENT != app_regs.REG_POS_TARGET)
        {
            send_position_event_counter = 2;
            
            if (app_regs.REG_POS_TARGET > app_regs.REG_POS_CURRENT)
            {
                clr_M_DIR;
                app_regs.REG_POS_CURRENT++;
            }
            else
            {   
                set_M_DIR;
                app_regs.REG_POS_CURRENT--;
            }
            
            set_M_STEP;
        }
    }
    
    if (send_position_event_counter)
    {
        if (++send_event_counter == 1000/10/4)  // 10 samples / second
        {
            send_event_counter = 0;
            send_position_event_counter--;
        
            if (app_regs.REG_EVNT_ENABLE & B_EVT3)
            {
                core_func_send_event(ADD_REG_POS_CURRENT, true);
            }
        }
    }        
}