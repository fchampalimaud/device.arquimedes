#ifndef _AS5048B_H_
#define _AS5048B_H_
#include <avr/io.h>

#define REG_OTP_ZERO_POSITION   0x16
#define REG_MAGNITUDE           0xFD
#define REG_ANGLE               0xFE

void initialize_rotary_encoder (uint8_t encoder_address);
void reset_angle (void);
int16_t read_angle (void);

#endif /* _AS5048B_H_ */