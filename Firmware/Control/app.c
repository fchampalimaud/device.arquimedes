#include "hwbp_core.h"
#include "hwbp_core_regs.h"
#include "hwbp_core_types.h"

#include "app.h"
#include "app_funcs.h"
#include "app_ios_and_regs.h"

#include "AS5048B.h"
#include "WS2812S.h"
#include "uart0.h"
#include "load_motor.h"

#define F_CPU 32000000
#include <util/delay.h>

/************************************************************************/
/* Declare application registers                                        */
/************************************************************************/
extern AppRegs app_regs;
extern uint8_t app_regs_type[];
extern uint16_t app_regs_n_elements[];
extern uint8_t *app_regs_pointer[];
extern void (*app_func_rd_pointer[])(void);
extern bool (*app_func_wr_pointer[])(void*);


/************************************************************************/
/* Initialize app                                                       */
/************************************************************************/
static const uint8_t default_device_name[] = "Archimedes";

void hwbp_app_initialize(void)
{
    /* Define versions */
    uint8_t hwH = 1;
    uint8_t hwL = 0;
    uint8_t fwH = 0;
    uint8_t fwL = 1;
    uint8_t ass = 0;
    
	/* Start core */
    core_func_start_core(1136, hwH, hwL, fwH, fwL, ass, (uint8_t*)(&app_regs), APP_NBYTES_OF_REG_BANK, APP_REGS_ADD_MAX - APP_REGS_ADD_MIN + 1, default_device_name);
}

/************************************************************************/
/* Handle if a catastrophic error occur                                 */
/************************************************************************/
void core_callback_catastrophic_error_detected(void)
{
	/* Remove power from the motor */
    set_M_SLEEP;
    
    /* Remove light from the LEDs */
    for (uint8_t i = 0; i < 9; i++)
        app_regs.REG_LEDS[i] = 0;
        
    /* Disable high and mid level interrupts */
    PMIC_CTRL = PMIC_RREN_bm | PMIC_LOLVLEN_bm;
        
    /* Udpate LEDs */
    update_3rgbs(app_regs.REG_LEDS, app_regs.REG_LEDS + 3, app_regs.REG_LEDS + 6);
        
    /* Re-enable high and mid level interrupts */
    PMIC_CTRL = PMIC_RREN_bm | PMIC_LOLVLEN_bm | PMIC_MEDLVLEN_bm | PMIC_HILVLEN_bm;

}

/************************************************************************/
/* General definitions                                                  */
/************************************************************************/
// #define NBYTES 23

/************************************************************************/
/* General used functions                                               */
/************************************************************************/
/* Load external functions if needed */
//#include "hwbp_app_pwm_gen_funcs.c"

/*
void update_enabled_pwmx(void)
{
	if (!core_bool_is_visual_enabled())
	return;
	
	if (!(app_regs.REG_CH_CONFEN & B_USEEN0) || ((app_regs.REG_CH_CONFEN & B_USEEN0) && (app_regs.REG_CH_ENABLE & B_EN0)))
		set_ENABLED_PWM0;
	else
		clr_ENABLED_PWM0;
}

ISR(PORTB_INT0_vect, ISR_NAKED)
{
	reti();
}
*/


/************************************************************************/
/* Initialization Callbacks                                             */
/************************************************************************/
void core_callback_1st_config_hw_after_boot(void)
{
	/* Initialize IOs */
	/* Don't delete this function!!! */
	init_ios();
    
    /* Initialize LEDs digital line */
    initialize_rgb();
    
    /* Initialize encoder's I2C and set current position as the ZERO */
    _delay_ms(50); // Startup time (20 ms)
    initialize_rotary_encoder(0x40);
    reset_angle();
    
    uart0_init(7, 0, false);
    uart0_enable();
}

void core_callback_reset_registers(void)
{
	/* Initialize registers */
	/*
	app_regs.REG_CH0_FREQ = 10.0;
	app_regs.REG_CH0_DUTYCYCLE = 50;
	
	if ((app_regs.REG_MODE0 & B_M0) == GM_USB_MODE)
	{
		app_regs.REG_OUT0 = 0;
	}

	if ((app_regs.REG_MODE1AND2 & B_M1AND2) == GM_USB_MODE)
	{
		app_regs.REG_OUT1 = 0;
		app_regs.REG_OUT2 = 0;
	}
	*/
    app_regs.REG_SET_DOUTS = 0;
    app_regs.REG_CLEAR_DOUTS = 0;
    
    for (uint8_t i = 0; i < 9; i++)
        app_regs.REG_LEDS[i] = 0;
    
    app_regs.REG_DOUT4_PULSE = 100;
    app_regs.REG_DOUT5_PULSE = 100;
    
    app_regs.REG_MOTOR_MICROSTEP = GM_STEP_FULL;
    
    app_regs.REG_POS_CURRENT = 0;
    app_regs.REG_POS_TARGET = 0;
    app_regs.REG_POS_0 = 500;
    app_regs.REG_POS_1 = 1000;
    app_regs.REG_POS_2 = 1500;
    app_regs.REG_POS_MAXIMUM = 1900;  
    
    app_regs.REG_LED_CONF0_WHICH = GM_LED_CONF_CENTER;
    app_regs.REG_LED_CONF0_GREEN = 0;
    app_regs.REG_LED_CONF0_RED = 0;
    app_regs.REG_LED_CONF0_BLUE = 0;
    app_regs.REG_LED_CONF1_WHICH = GM_LED_CONF_CENTER;
    app_regs.REG_LED_CONF1_GREEN = 0;
    app_regs.REG_LED_CONF1_RED = 0;
    app_regs.REG_LED_CONF1_BLUE = 0;
    app_regs.REG_LED_CONF2_WHICH = GM_LED_CONF_CENTER;
    app_regs.REG_LED_CONF2_GREEN = 0;
    app_regs.REG_LED_CONF2_RED = 0;
    app_regs.REG_LED_CONF2_BLUE = 0;
    app_regs.REG_LED_CONF3_WHICH = GM_LED_CONF_CENTER;
    app_regs.REG_LED_CONF3_GREEN = 0;
    app_regs.REG_LED_CONF3_RED = 0;
    app_regs.REG_LED_CONF3_BLUE = 0;
    app_regs.REG_LED_CONF4_WHICH = GM_LED_CONF_CENTER;
    app_regs.REG_LED_CONF4_GREEN = 0;
    app_regs.REG_LED_CONF4_RED = 0;
    app_regs.REG_LED_CONF4_BLUE = 0;    
    app_regs.REG_LED_CONF5_WHICH = GM_LED_CONF_CENTER;
    app_regs.REG_LED_CONF5_GREEN = 0;
    app_regs.REG_LED_CONF5_RED = 0;
    app_regs.REG_LED_CONF5_BLUE = 0;    
    app_regs.REG_LED_CONF6_WHICH = GM_LED_CONF_CENTER;
    app_regs.REG_LED_CONF6_GREEN = 0;
    app_regs.REG_LED_CONF6_RED = 0;
    app_regs.REG_LED_CONF6_BLUE = 0;    
    app_regs.REG_LED_CONF7_WHICH = GM_LED_CONF_CENTER;
    app_regs.REG_LED_CONF7_GREEN = 0;
    app_regs.REG_LED_CONF7_RED = 0;
    app_regs.REG_LED_CONF7_BLUE = 0;    
    
    app_regs.REG_TH_STANDBY = 28;   // 0.61�
    app_regs.REG_TH_0 = 457;        // 10.01�
    app_regs.REG_TH_1 = 914;        // 20.02�
    app_regs.REG_TH_2 = 1370;       // 30.00�
    app_regs.REG_TH_3 = 1827;       // 40.01�
    app_regs.REG_TH_STANDBY_MS = 10;
    app_regs.REG_TH_0_ON = 10;
    app_regs.REG_TH_0_OFF = 10;
    app_regs.REG_TH_1_ON = 10;
    app_regs.REG_TH_1_OFF = 10;
    app_regs.REG_TH_2_ON = 10;
    app_regs.REG_TH_2_OFF = 10;
    app_regs.REG_TH_3_ON = 10;
    app_regs.REG_TH_3_OFF = 10;
    
    app_regs.REG_TH_0_LED_CONF = GM_TH_LED_CONF_NOT_USED;
    app_regs.REG_TH_1_LED_CONF = GM_TH_LED_CONF_NOT_USED;
    app_regs.REG_TH_2_LED_CONF = GM_TH_LED_CONF_NOT_USED;
    app_regs.REG_TH_3_LED_CONF = GM_TH_LED_CONF_NOT_USED;
    
    app_regs.REG_SAMPLE_RATE = GM_ACQ_1000HZ;
    
    app_regs.REG_CONF_DI0 = GM_DI0_NOT_USED;
    app_regs.REG_CONF_DI1 = GM_DI1_NOT_USED;
    app_regs.REG_CONF_DI2 = GM_DI2_NOT_USED;
    app_regs.REG_CONF_DI3 = GM_DI3_NOT_USED;    

    app_regs.REG_CONF_DO0 = GM_DO0_QUIET;
    app_regs.REG_CONF_DO1 = GM_DO1_TH0;
    app_regs.REG_CONF_DO2 = GM_DO2_TH1;
    app_regs.REG_CONF_DO3 = GM_DO3_TH2;
    app_regs.REG_CONF_DO4 = GM_DO4_TH3;
    app_regs.REG_CONF_DO5 = GM_DO5_SOFT;
    
    app_regs.REG_EVNT_ENABLE = B_EVT3 | B_EVT2 | B_EVT1 | B_EVT0;
}

void core_callback_registers_were_reinitialized(void)
{	
    app_regs.REG_SET_DOUTS = 0;
	app_regs.REG_CLEAR_DOUTS = 0;
    
    app_regs.REG_THRESHOLDS = 0;
    
    app_regs.REG_DATA[0] = 0;
    app_regs.REG_DATA[1] = 0;
    
    app_regs.REG_INPUTS = 0;
    
    uint8_t aux = B_RST_MOTOR;
    app_regs.REG_RESET_MOTOR = 0;
    app_write_REG_RESET_MOTOR(&aux);
    
    app_write_REG_MOTOR_MICROSTEP(&app_regs.REG_MOTOR_MICROSTEP);
    clr_M_SLEEP;
    
    app_regs.REG_POS_CURRENT = 0;
    app_regs.REG_POS_TARGET = 0;
    
    update_3rgbs(app_regs.REG_LEDS, app_regs.REG_LEDS + 3, app_regs.REG_LEDS + 6);
}

/************************************************************************/
/* Callbacks: Visualization                                             */
/************************************************************************/
void core_callback_visualen_to_on(void)
{
	/* Update channels enable indicators */
	//update_enabled_pwmx();
}

void core_callback_visualen_to_off(void)
{
	/* Clear all the enabled indicators */
}

/************************************************************************/
/* Callbacks: Change on the operation mode                              */
/************************************************************************/
void core_callback_device_to_standby(void) {}
void core_callback_device_to_active(void) {}
void core_callback_device_to_enchanced_active(void) {}
void core_callback_device_to_speed(void) {}

/************************************************************************/
/* Callbacks: 1 ms timer                                                */
/************************************************************************/
/* Flag to trigger the LEDS update based on register app_regs.REG_LEDS */
bool update_leds = false;

/* Control of the sample frequency */
static uint8_t sample_freq_ctrl = 0;

/* Data from Interface board                                        */
/* byte 0: [0] [IN03]  [IN02]  [IN01] [ADC4] [ADC3] [ADC2] [ADC1]    */
/* byte 1: [1] [ADC11] [ADC10] [ADC9] [ADC8] [ADC7] [ADC6] [ADC5]    */
uint8_t byte0;
uint8_t byte1;

void uart0_rcv_byte_callback(uint8_t byte)
{
    if (byte & 0x80)
    {
        byte1 = byte;
    }        
    else
    {
        byte0 = byte;
    }        
}

#define xmit_outputs(a) UART0_UART.DATA = a;

uint8_t prev_inputs = 0;

void process_inputs (void)
{   
    uint8_t inputs_state;
    inputs_state = ((byte0 >> 3) & 0x0E) | (get_IN0 ? 1 : 0);
    
    for (uint8_t i = 4; i != 0; i--)
    {
        uint8_t compare_value = (1 << (i-1));
        
        if ((inputs_state & compare_value) != (prev_inputs & compare_value))
        {
            /* Rising edge detected */
            if (inputs_state & compare_value)
            {
                /* Check if the lever should be hidden */
                if (*((&app_regs.REG_CONF_DI0) + i-1) == GM_DI0_HIDE_LEVER)
                {
                    app_regs.REG_HIDE_LEVER = 1;
                    app_write_REG_HIDE_LEVER(&app_regs.REG_HIDE_LEVER);
                }
                if (*((&app_regs.REG_CONF_DI0) + i-1) == GM_DI1_MOTOR_POSITION0)
                {
                    /* Input 0 didn't change motor position */
                    if (i == B_IN0)
                        return;
                    
                    uint16_t motor_position = *((&app_regs.REG_POS_0) + i-1 - 1);
                    app_write_REG_POS_TARGET(&motor_position);
                }
                else if ((compare_value == B_IN2) && (app_regs.REG_CONF_DI2 == GM_DI2_SET_DO4) && (app_regs.REG_CONF_DO4 == GM_DO4_PULSE))
                {
                    uint8_t aux = (1<<4);
                    app_write_REG_SET_DOUTS(&aux);
                }
                else if ((compare_value == B_IN3) && (app_regs.REG_CONF_DI3 == GM_DI3_SET_DO5) && (app_regs.REG_CONF_DO5 == GM_DO5_PULSE))
                {
                    uint8_t aux = (1<<5);
                    app_write_REG_SET_DOUTS(&aux);
                }
                else if ((*((&app_regs.REG_CONF_DI0) + i-1) == GM_DI0_SET_CONF0) || (*((&app_regs.REG_CONF_DI0) + i-1) == GM_DI0_SET_CLR_CONF0))
                {
                    update_leds = true;
                    
                    uint8_t color_pointer = *((&app_regs.REG_LED_CONF0_WHICH) + (i-1) * 4) * 3;
                    app_regs.REG_LEDS[color_pointer + 0] = *((&app_regs.REG_LED_CONF0_GREEN) + (i-1) * 4);
                    app_regs.REG_LEDS[color_pointer + 1] = *((&app_regs.REG_LED_CONF0_RED) + (i-1) * 4);
                    app_regs.REG_LEDS[color_pointer + 2] = *((&app_regs.REG_LED_CONF0_BLUE) + (i-1) * 4);
                }
            }
            /* Falling edge detected */
            else
            {   
                /* Check if the lever should appear again */
                if (*((&app_regs.REG_CONF_DI0) + i-1) == GM_DI0_HIDE_LEVER)
                {
                    app_regs.REG_HIDE_LEVER = 0;
                    app_write_REG_HIDE_LEVER(&app_regs.REG_HIDE_LEVER);
                }
                else if (*((&app_regs.REG_CONF_DI0) + i-1) == GM_DI0_SET_CLR_CONF0)
                {
                    update_leds = true;
                    
                    uint8_t color_pointer = *((&app_regs.REG_LED_CONF0_WHICH) + (i-1) * 4) * 3;
                    app_regs.REG_LEDS[color_pointer + 0] = 0;
                    app_regs.REG_LEDS[color_pointer + 1] = 0;
                    app_regs.REG_LEDS[color_pointer + 2] = 0;
                }
            }
        }
    }
    
    if (inputs_state != prev_inputs)
    {
        app_regs.REG_INPUTS = inputs_state;
        if (app_regs.REG_EVNT_ENABLE & B_EVT2)
            core_func_send_event(ADD_REG_INPUTS, true);
    }
    
    prev_inputs = inputs_state;
}

/* Counter for the thresholds */
uint16_t thresholds_up_counter = 0;
uint16_t thresholds_down_counter = 0;

/* Pointer to the LED that TH0 will shutdown */
uint8_t *th0_led_rgb_pointer;

void threshold_crossed_up (uint8_t new_threshold)
{
    thresholds_up_counter = 0;
    
    app_regs.REG_THRESHOLDS |= new_threshold ;
    
    if (app_regs.REG_EVNT_ENABLE & B_EVT0)
        core_func_send_event(ADD_REG_THRESHOLDS, true);
    
    uint8_t th_led_conf_index;
    for (uint8_t i = 5; i !=0; i--)
    {
        if (new_threshold & (1<<(i-1)))
        {
            th_led_conf_index = i - 1;
        }
    }
    
    if (*((&app_regs.REG_CONF_DO0) + th_led_conf_index) == GM_DO0_QUIET)
    {
        app_write_REG_SET_DOUTS(&new_threshold);
    }
    
    if (new_threshold == B_QUIET)
        return;
    
    th_led_conf_index--;
    if ((*((&app_regs.REG_TH_0_LED_CONF) + th_led_conf_index) & MSK_TH_LED_CONF) != GM_TH_LED_CONF_NOT_USED)
    {
        update_leds = true;
        
        if (*((&app_regs.REG_TH_0_LED_CONF) + th_led_conf_index) & B_CLR_THE_OTHER_LEDS)
        {
            for (uint8_t i = 9; i != 0; i--)
            {
                app_regs.REG_LEDS[i-1] = 0;
            }                
        }
        
        uint8_t *led_conf_which_index = (uint8_t*)(&app_regs.REG_LED_CONF0_WHICH) + (((*((&app_regs.REG_TH_0_LED_CONF) + th_led_conf_index) - 1) & MSK_TH_LED_CONF) * 4);
        
        for (uint8_t i = 0; i < 3; i++)
            app_regs.REG_LEDS[i + *led_conf_which_index * 3] = *(led_conf_which_index + 1 + i);
        
        if (new_threshold == B_TH0)
        {
            th0_led_rgb_pointer = &app_regs.REG_LEDS[*led_conf_which_index * 3];
        }
        
    }        
}

void threshold_crossed_down (uint8_t previous_threshold)
{
    thresholds_down_counter = 0;
    
    app_regs.REG_THRESHOLDS &= (~previous_threshold);
    
    if (app_regs.REG_EVNT_ENABLE & B_EVT0)
        core_func_send_event(ADD_REG_THRESHOLDS, true);

    uint8_t th_led_conf_index;
    for (uint8_t i = 5; i !=0; i--)
    {
        if (previous_threshold & (1<<(i-1)))
        {
            th_led_conf_index = i-1;
        }
    }
    
    if (*((&app_regs.REG_CONF_DO0) + th_led_conf_index) == GM_DO0_QUIET)
    {
        app_write_REG_CLEAR_DOUTS(&previous_threshold);
    }
        
    th_led_conf_index--;
    th_led_conf_index--;
    
    if (!(previous_threshold & (B_QUIET | B_TH0)))
    {
        if ((*((&app_regs.REG_TH_0_LED_CONF) + th_led_conf_index) & MSK_TH_LED_CONF) != GM_TH_LED_CONF_NOT_USED)
        {
            update_leds = true;
        
            if (*((&app_regs.REG_TH_0_LED_CONF) + th_led_conf_index) & B_CLR_THE_OTHER_LEDS)
            {
                for (uint8_t i = 9; i != 0; i--)
                {
                    app_regs.REG_LEDS[i-1] = 0;
                }
            }
        
            uint8_t *led_conf_which_index = (uint8_t*)(&app_regs.REG_LED_CONF0_WHICH) + (((*((&app_regs.REG_TH_0_LED_CONF) + th_led_conf_index) - 1) & MSK_TH_LED_CONF) * 4);
        
            for (uint8_t i = 0; i < 3; i++)
                app_regs.REG_LEDS[i + *led_conf_which_index * 3] = *(led_conf_which_index + 1 + i);
        }
    }
    else if (previous_threshold == B_TH0)
    {
        if (app_regs.REG_TH_0_LED_CONF & B_CLR_TH0_LED_CONF)
        {
            update_leds = true;
            
            for (uint8_t i = 3; i != 0; i--)
            {
                *(th0_led_rgb_pointer + i - 1) = 0;
            }
        }
        else if (app_regs.REG_TH_0_LED_CONF & B_CLR_THE_OTHER_LEDS)
        {
            update_leds = true;
            
            for (uint8_t i = 9; i != 0; i--)
            {
                app_regs.REG_LEDS[i-1] = 0;
            }
        }
    }        
    
}

extern uint16_t do4_pulse_counter;
extern uint16_t do5_pulse_counter;

void core_callback_t_before_exec(void)
{    
    /* Ask byte0 and byte1 from Interface board for the next iteration */
    tgl_UCUC;

    /* Acquire sample each millisecond */
    if (!(sample_freq_ctrl & 1))
    {
        app_regs.REG_DATA[0] = read_angle();
        app_regs.REG_DATA[1] = ((uint16_t)(byte1 & 0x7F)) << 4 | (byte0 & 0x0F);
        
        if (app_regs.REG_DATA[0] & 0x2000 /* > 8192 */)
        {
            app_regs.REG_DATA[0] = (0x3FFF /* 2^14 - 1 */ - app_regs.REG_DATA[0]) * -1;
        }
        
        /* C2 of the register */
        //app_regs.REG_DATA[0] = ~app_regs.REG_DATA[0] + 1;
    }
    
    /* Check if sample should be sent */    
    if (!(sample_freq_ctrl & app_regs.REG_SAMPLE_RATE))
    {
        if (app_regs.REG_EVNT_ENABLE & B_EVT1)
        {
            if (app_regs.REG_TH_STANDBY & B_TX_WHEN_NOT_QUIET)
            {
                if (app_regs.REG_THRESHOLDS & B_QUIET)
                {
                    core_func_send_event(ADD_REG_DATA, true);
                }
            }
            else
            {
                core_func_send_event(ADD_REG_DATA, true);
            }                
        }        
    }
    
    sample_freq_ctrl++;
    
    /* Test threshold UP */
    if (app_regs.REG_DATA[0] > 0)
    {
        switch (app_regs.REG_THRESHOLDS)
        {
            case 0:
                if (app_regs.REG_DATA[0] > (app_regs.REG_TH_STANDBY & MSK_QUIET_TH))
                {
                    if (++thresholds_up_counter == (app_regs.REG_TH_STANDBY & MSK_QUIET_TH))
                        threshold_crossed_up(B_QUIET);
                }
                else
                    thresholds_up_counter = 0;
            
    	        break;
    
            case B_QUIET:
                if (app_regs.REG_DATA[0] > app_regs.REG_TH_0)
                {
                    if (++thresholds_up_counter == app_regs.REG_TH_0_ON)
                        threshold_crossed_up(B_TH0);
                }
                else
                    thresholds_up_counter = 0;
        
                break;

            case B_QUIET | B_TH0:
                if (app_regs.REG_DATA[0] > app_regs.REG_TH_1)
                {
                    if (++thresholds_up_counter == app_regs.REG_TH_1_ON)
                        threshold_crossed_up(B_TH1);
                }
                else
                    thresholds_up_counter = 0;
            
                break;
    
            case B_QUIET | B_TH0 | B_TH1:
                if (app_regs.REG_DATA[0] > app_regs.REG_TH_2)
                {
                    if (++thresholds_up_counter == app_regs.REG_TH_2_ON)
                    threshold_crossed_up(B_TH2);
                }
                else
                thresholds_up_counter = 0;
        
                break;
        
            case B_QUIET | B_TH0 | B_TH1 | B_TH2:
                if (app_regs.REG_DATA[0] > app_regs.REG_TH_3)
                {
                    if (++thresholds_up_counter == app_regs.REG_TH_3_ON)
                        threshold_crossed_up(B_TH3);
                }
                else
                thresholds_up_counter = 0;
        
                break;
        }
    }
    
     /* Test threshold DOWN */
     if (app_regs.REG_DATA[0] > 0)
     {
         switch (app_regs.REG_THRESHOLDS)
         {
             case B_QUIET | B_TH0 | B_TH1 | B_TH2 | B_TH3:
                 if (app_regs.REG_DATA[0] < app_regs.REG_TH_3)
                 {
                     if (++thresholds_down_counter == app_regs.REG_TH_3_OFF)
                         threshold_crossed_down(B_TH3);
                 }
                 else
                    thresholds_down_counter = 0;
             
                 break;
            
            case B_QUIET | B_TH0 | B_TH1 | B_TH2:
                 if (app_regs.REG_DATA[0] < app_regs.REG_TH_2)
                 {
                     if (++thresholds_down_counter == app_regs.REG_TH_2_OFF)
                         threshold_crossed_down(B_TH2);
                 }
                 else
                    thresholds_down_counter = 0;
                 
                 break;
                 
            case B_QUIET | B_TH0 | B_TH1:
                 if (app_regs.REG_DATA[0] < app_regs.REG_TH_1)
                 {
                     if (++thresholds_down_counter == app_regs.REG_TH_1_OFF)
                        threshold_crossed_down(B_TH1);
                 }
                 else
                     thresholds_down_counter = 0;
                 
                 break;
                     
            case B_QUIET | B_TH0:
                if (app_regs.REG_DATA[0] < app_regs.REG_TH_0)
                {
                    if (++thresholds_down_counter == app_regs.REG_TH_0_OFF)
                        threshold_crossed_down(B_TH0);
                }
               else
                    thresholds_down_counter = 0;   
                 
                 break;         
                   
            case B_QUIET:
                    if (app_regs.REG_DATA[0] < (app_regs.REG_TH_STANDBY & MSK_QUIET_TH))
                    {
                        if (++thresholds_down_counter ==(app_regs.REG_TH_STANDBY & MSK_QUIET_TH))
                            threshold_crossed_down(B_QUIET);
                    }
                    else
                        thresholds_down_counter = 0;
                 
                 break;
        }
    }
    
    process_inputs();
    
    if (update_leds)
    {
        update_leds = false;
        
        /* Disable high and mid level interrupts */
        PMIC_CTRL = PMIC_RREN_bm | PMIC_LOLVLEN_bm;
        		
        /* Udpate LEDs */
        update_3rgbs(app_regs.REG_LEDS, app_regs.REG_LEDS + 3, app_regs.REG_LEDS + 6);
        		
        /* Re-enable high and mid level interrupts */
        PMIC_CTRL = PMIC_RREN_bm | PMIC_LOLVLEN_bm | PMIC_MEDLVLEN_bm | PMIC_HILVLEN_bm;
    }
    
    if (do4_pulse_counter)
    {
        if (--do4_pulse_counter == 0)
        {
            uint8_t aux = B_CLR_DO4;
            app_write_REG_CLEAR_DOUTS(&aux);
        }
    }
    
    if (do5_pulse_counter)
    {
        if (--do5_pulse_counter == 0)
        {
            uint8_t aux = B_CLR_DO5;
            app_write_REG_CLEAR_DOUTS(&aux);
        }
    }
}


void core_callback_t_after_exec(void) {}
void core_callback_t_new_second(void)
{
    sample_freq_ctrl = 0;
}
void core_callback_t_500us(void) {}
void core_callback_t_1ms(void) { load_motor(); }

/************************************************************************/
/* Callbacks: uart control                                              */
/************************************************************************/
void core_callback_uart_rx_before_exec(void) {}
void core_callback_uart_rx_after_exec(void) {}
void core_callback_uart_tx_before_exec(void) {}
void core_callback_uart_tx_after_exec(void) {}
void core_callback_uart_cts_before_exec(void) {}
void core_callback_uart_cts_after_exec(void) {}

/************************************************************************/
/* Callbacks: Read app register                                         */
/************************************************************************/
bool core_read_app_register(uint8_t add, uint8_t type)
{
	/* Check if it will not access forbidden memory */
	if (add < APP_REGS_ADD_MIN || add > APP_REGS_ADD_MAX)
		return false;
	
	/* Check if type matches */
	if (app_regs_type[add-APP_REGS_ADD_MIN] != type)
		return false;
	
	/* Receive data */
	(*app_func_rd_pointer[add-APP_REGS_ADD_MIN])();	

	/* Return success */
	return true;
}

/************************************************************************/
/* Callbacks: Write app register                                        */
/************************************************************************/
bool core_write_app_register(uint8_t add, uint8_t type, uint8_t * content, uint16_t n_elements)
{
	/* Check if it will not access forbidden memory */
	if (add < APP_REGS_ADD_MIN || add > APP_REGS_ADD_MAX)
		return false;
	
	/* Check if type matches */
	if (app_regs_type[add-APP_REGS_ADD_MIN] != type)
		return false;

	/* Check if the number of elements matches */
	if (app_regs_n_elements[add-APP_REGS_ADD_MIN] != n_elements)
		return false;

	/* Process data and return false if write is not allowed or contains errors */
	return (*app_func_wr_pointer[add-APP_REGS_ADD_MIN])(content);
}