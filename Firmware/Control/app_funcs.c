#include "app_funcs.h"
#include "app_ios_and_regs.h"
#include "hwbp_core.h"

#include "AS5048B.h"
#include "WS2812S.h"
#include "uart0.h"
extern bool update_leds;

/************************************************************************/
/* Create pointers to functions                                         */
/************************************************************************/
extern AppRegs app_regs;

void (*app_func_rd_pointer[])(void) = {
	&app_read_REG_THRESHOLDS,
	&app_read_REG_DATA,
	&app_read_REG_INPUTS,
	&app_read_REG_RESERVED0,
	&app_read_REG_RESERVED1,
	&app_read_REG_FEEDBACK,
	&app_read_REG_RESERVED2,
	&app_read_REG_RESET_ANGLE,
	&app_read_REG_RESET_MOTOR,
	&app_read_REG_HIDE_LEVER,
	&app_read_REG_SET_DOUTS,
	&app_read_REG_CLEAR_DOUTS,
	&app_read_REG_EN_LED_CONFS,
	&app_read_REG_DIS_LED_CONFS,
	&app_read_REG_LEDS,
	&app_read_REG_DOUT4_PULSE,
	&app_read_REG_DOUT5_PULSE,
	&app_read_REG_RESERVED3,
	&app_read_REG_SAMPLE_RATE,
	&app_read_REG_USE_FEEDBACK,
	&app_read_REG_MOTOR_MICROSTEP,
	&app_read_REG_RESERVED4,
	&app_read_REG_RESERVED5,
	&app_read_REG_POS_CURRENT,
	&app_read_REG_POS_TARGET,
	&app_read_REG_POS_0,
	&app_read_REG_POS_1,
	&app_read_REG_POS_2,
	&app_read_REG_POS_MAXIMUM,
	&app_read_REG_RESERVED7,
	&app_read_REG_LED_CONF0_WHICH,
	&app_read_REG_LED_CONF0_GREEN,
	&app_read_REG_LED_CONF0_RED,
	&app_read_REG_LED_CONF0_BLUE,
	&app_read_REG_LED_CONF1_WHICH,
	&app_read_REG_LED_CONF1_GREEN,
	&app_read_REG_LED_CONF1_RED,
	&app_read_REG_LED_CONF1_BLUE,
	&app_read_REG_LED_CONF2_WHICH,
	&app_read_REG_LED_CONF2_GREEN,
	&app_read_REG_LED_CONF2_RED,
	&app_read_REG_LED_CONF2_BLUE,
	&app_read_REG_LED_CONF3_WHICH,
	&app_read_REG_LED_CONF3_GREEN,
	&app_read_REG_LED_CONF3_RED,
	&app_read_REG_LED_CONF3_BLUE,
	&app_read_REG_LED_CONF4_WHICH,
	&app_read_REG_LED_CONF4_GREEN,
	&app_read_REG_LED_CONF4_RED,
	&app_read_REG_LED_CONF4_BLUE,
	&app_read_REG_LED_CONF5_WHICH,
	&app_read_REG_LED_CONF5_GREEN,
	&app_read_REG_LED_CONF5_RED,
	&app_read_REG_LED_CONF5_BLUE,
	&app_read_REG_LED_CONF6_WHICH,
	&app_read_REG_LED_CONF6_GREEN,
	&app_read_REG_LED_CONF6_RED,
	&app_read_REG_LED_CONF6_BLUE,
	&app_read_REG_LED_CONF7_WHICH,
	&app_read_REG_LED_CONF7_GREEN,
	&app_read_REG_LED_CONF7_RED,
	&app_read_REG_LED_CONF7_BLUE,
	&app_read_REG_RESERVED8,
	&app_read_REG_RESERVED9,
	&app_read_REG_TH_STANDBY,
	&app_read_REG_TH_0,
	&app_read_REG_TH_1,
	&app_read_REG_TH_2,
	&app_read_REG_TH_3,
	&app_read_REG_TH_STANDBY_MS,
	&app_read_REG_TH_0_ON,
	&app_read_REG_TH_0_OFF,
	&app_read_REG_TH_1_ON,
	&app_read_REG_TH_1_OFF,
	&app_read_REG_TH_2_ON,
	&app_read_REG_TH_2_OFF,
	&app_read_REG_TH_3_ON,
	&app_read_REG_TH_3_OFF,
	&app_read_REG_TH_0_LED_CONF,
	&app_read_REG_TH_1_LED_CONF,
	&app_read_REG_TH_2_LED_CONF,
	&app_read_REG_TH_3_LED_CONF,
	&app_read_REG_RESERVED10,
	&app_read_REG_RESERVED11,
	&app_read_REG_CONF_DI0,
	&app_read_REG_CONF_DI1,
	&app_read_REG_CONF_DI2,
	&app_read_REG_CONF_DI3,
	&app_read_REG_RESERVED12,
	&app_read_REG_RESERVED13,
	&app_read_REG_CONF_DO0,
	&app_read_REG_CONF_DO1,
	&app_read_REG_CONF_DO2,
	&app_read_REG_CONF_DO3,
	&app_read_REG_CONF_DO4,
	&app_read_REG_CONF_DO5,
	&app_read_REG_RESERVED14,
	&app_read_REG_RESERVED15,
	&app_read_REG_EVNT_ENABLE
};

bool (*app_func_wr_pointer[])(void*) = {
	&app_write_REG_THRESHOLDS,
	&app_write_REG_DATA,
	&app_write_REG_INPUTS,
	&app_write_REG_RESERVED0,
	&app_write_REG_RESERVED1,
	&app_write_REG_FEEDBACK,
	&app_write_REG_RESERVED2,
	&app_write_REG_RESET_ANGLE,
	&app_write_REG_RESET_MOTOR,
	&app_write_REG_HIDE_LEVER,
	&app_write_REG_SET_DOUTS,
	&app_write_REG_CLEAR_DOUTS,
	&app_write_REG_EN_LED_CONFS,
	&app_write_REG_DIS_LED_CONFS,
	&app_write_REG_LEDS,
	&app_write_REG_DOUT4_PULSE,
	&app_write_REG_DOUT5_PULSE,
	&app_write_REG_RESERVED3,
	&app_write_REG_SAMPLE_RATE,
	&app_write_REG_USE_FEEDBACK,
	&app_write_REG_MOTOR_MICROSTEP,
	&app_write_REG_RESERVED4,
	&app_write_REG_RESERVED5,
	&app_write_REG_POS_CURRENT,
	&app_write_REG_POS_TARGET,
	&app_write_REG_POS_0,
	&app_write_REG_POS_1,
	&app_write_REG_POS_2,
	&app_write_REG_POS_MAXIMUM,
	&app_write_REG_RESERVED7,
	&app_write_REG_LED_CONF0_WHICH,
	&app_write_REG_LED_CONF0_GREEN,
	&app_write_REG_LED_CONF0_RED,
	&app_write_REG_LED_CONF0_BLUE,
	&app_write_REG_LED_CONF1_WHICH,
	&app_write_REG_LED_CONF1_GREEN,
	&app_write_REG_LED_CONF1_RED,
	&app_write_REG_LED_CONF1_BLUE,
	&app_write_REG_LED_CONF2_WHICH,
	&app_write_REG_LED_CONF2_GREEN,
	&app_write_REG_LED_CONF2_RED,
	&app_write_REG_LED_CONF2_BLUE,
	&app_write_REG_LED_CONF3_WHICH,
	&app_write_REG_LED_CONF3_GREEN,
	&app_write_REG_LED_CONF3_RED,
	&app_write_REG_LED_CONF3_BLUE,
	&app_write_REG_LED_CONF4_WHICH,
	&app_write_REG_LED_CONF4_GREEN,
	&app_write_REG_LED_CONF4_RED,
	&app_write_REG_LED_CONF4_BLUE,
	&app_write_REG_LED_CONF5_WHICH,
	&app_write_REG_LED_CONF5_GREEN,
	&app_write_REG_LED_CONF5_RED,
	&app_write_REG_LED_CONF5_BLUE,
	&app_write_REG_LED_CONF6_WHICH,
	&app_write_REG_LED_CONF6_GREEN,
	&app_write_REG_LED_CONF6_RED,
	&app_write_REG_LED_CONF6_BLUE,
	&app_write_REG_LED_CONF7_WHICH,
	&app_write_REG_LED_CONF7_GREEN,
	&app_write_REG_LED_CONF7_RED,
	&app_write_REG_LED_CONF7_BLUE,
	&app_write_REG_RESERVED8,
	&app_write_REG_RESERVED9,
	&app_write_REG_TH_STANDBY,
	&app_write_REG_TH_0,
	&app_write_REG_TH_1,
	&app_write_REG_TH_2,
	&app_write_REG_TH_3,
	&app_write_REG_TH_STANDBY_MS,
	&app_write_REG_TH_0_ON,
	&app_write_REG_TH_0_OFF,
	&app_write_REG_TH_1_ON,
	&app_write_REG_TH_1_OFF,
	&app_write_REG_TH_2_ON,
	&app_write_REG_TH_2_OFF,
	&app_write_REG_TH_3_ON,
	&app_write_REG_TH_3_OFF,
	&app_write_REG_TH_0_LED_CONF,
	&app_write_REG_TH_1_LED_CONF,
	&app_write_REG_TH_2_LED_CONF,
	&app_write_REG_TH_3_LED_CONF,
	&app_write_REG_RESERVED10,
	&app_write_REG_RESERVED11,
	&app_write_REG_CONF_DI0,
	&app_write_REG_CONF_DI1,
	&app_write_REG_CONF_DI2,
	&app_write_REG_CONF_DI3,
	&app_write_REG_RESERVED12,
	&app_write_REG_RESERVED13,
	&app_write_REG_CONF_DO0,
	&app_write_REG_CONF_DO1,
	&app_write_REG_CONF_DO2,
	&app_write_REG_CONF_DO3,
	&app_write_REG_CONF_DO4,
	&app_write_REG_CONF_DO5,
	&app_write_REG_RESERVED14,
	&app_write_REG_RESERVED15,
	&app_write_REG_EVNT_ENABLE
};


/************************************************************************/
/* REG_THRESHOLDS                                                       */
/************************************************************************/
void app_read_REG_THRESHOLDS(void) {}
bool app_write_REG_THRESHOLDS(void *a) { return false; }


/************************************************************************/
/* REG_DATA                                                             */
/************************************************************************/
// This register is an array with 2 positions
void app_read_REG_DATA(void) {}
bool app_write_REG_DATA(void *a) { return false; }


/************************************************************************/
/* REG_INPUTS                                                           */
/************************************************************************/
void app_read_REG_INPUTS(void) {}
bool app_write_REG_INPUTS(void *a) { return false; }


/************************************************************************/
/* REG_RESERVED0                                                        */
/************************************************************************/
void app_read_REG_RESERVED0(void) {}
bool app_write_REG_RESERVED0(void *a) { return true; }


/************************************************************************/
/* REG_RESERVED1                                                        */
/************************************************************************/
void app_read_REG_RESERVED1(void)
{
	//app_regs.REG_RESERVED1 = 0;

}

bool app_write_REG_RESERVED1(void *a)
{
	uint8_t reg = *((uint8_t*)a);

	app_regs.REG_RESERVED1 = reg;
	return true;
}


/************************************************************************/
/* REG_FEEDBACK                                                         */
/************************************************************************/
void app_read_REG_FEEDBACK(void)
{
	//app_regs.REG_FEEDBACK = 0;

}

bool app_write_REG_FEEDBACK(void *a)
{
	uint16_t reg = *((uint16_t*)a);

	app_regs.REG_FEEDBACK = reg;
	return true;
}


/************************************************************************/
/* REG_RESERVED2                                                        */
/************************************************************************/
void app_read_REG_RESERVED2(void)
{
	//app_regs.REG_RESERVED2 = 0;

}

bool app_write_REG_RESERVED2(void *a)
{
	uint8_t reg = *((uint8_t*)a);

	app_regs.REG_RESERVED2 = reg;
	return true;
}


/************************************************************************/
/* REG_RESET_ANGLE                                                      */
/************************************************************************/
void app_read_REG_RESET_ANGLE(void) {}
bool app_write_REG_RESET_ANGLE(void *a)
{
	if (*((uint8_t*)a) == B_RST_ANGLE)
    {
        reset_angle();
	    return true;
    }
    else
    {
        return false;
    }       
}


/************************************************************************/
/* REG_RESET_MOTOR                                                      */
/************************************************************************/
uint16_t reset_motor_traveling;

void app_read_REG_RESET_MOTOR(void) {}
bool app_write_REG_RESET_MOTOR(void *a)
{
	uint8_t reg = *((uint8_t*)a);
    
    if (reg & ~B_RST_MOTOR)
        return false;
    
    if (app_regs.REG_RESET_MOTOR == B_RST_MOTOR)
    {
        return true;
    }
    else
    {
        if (reg == B_RST_MOTOR)
        {
            reset_motor_traveling = app_regs.REG_POS_MAXIMUM + (app_regs.REG_POS_MAXIMUM >> 1);  // Multiplied by 1.5
            
            app_regs.REG_RESET_MOTOR = B_RST_MOTOR;
            app_regs.REG_POS_CURRENT = 0;
            app_regs.REG_POS_TARGET = 0;
        }
    }
    
	return true;
}


/************************************************************************/
/* REG_HIDE_LEVER                                                       */
/************************************************************************/
void app_read_REG_HIDE_LEVER(void)
{
	//app_regs.REG_HIDE_LEVER = 0;

}

bool app_write_REG_HIDE_LEVER(void *a)
{
	uint8_t reg = *((uint8_t*)a);

	app_regs.REG_HIDE_LEVER = reg;
	return true;
}


/************************************************************************/
/* REG_SET_DOUTS                                                        */
/************************************************************************/
uint8_t digital_outs = 0;
uint16_t do4_pulse_counter;
uint16_t do5_pulse_counter;

void app_read_REG_SET_DOUTS(void) {}
bool app_write_REG_SET_DOUTS(void *a)
{
	digital_outs |= *((uint8_t*)a);
    uart0_xmit_now_byte(digital_outs);
    if (digital_outs & B_SET_DO0)
        set_OUT0;
    
    if ((digital_outs & B_SET_DO4) && (app_regs.REG_CONF_DO4 == GM_DO4_PULSE))
    {
        do4_pulse_counter = (uint16_t)(app_regs.REG_DOUT4_PULSE) * 2 + 1;
    }
    
    
    if ((digital_outs & B_SET_DO5) && (app_regs.REG_CONF_DO5 == GM_DO5_PULSE))
    {
        do5_pulse_counter = (uint16_t)(app_regs.REG_DOUT5_PULSE) * 2 + 1;
    }
    
	return true;
}


/************************************************************************/
/* REG_CLEAR_DOUTS                                                      */
/************************************************************************/
void app_read_REG_CLEAR_DOUTS(void) {}
bool app_write_REG_CLEAR_DOUTS(void *a)
{
    digital_outs &= ~(*((uint8_t*)a));
    uart0_xmit_now_byte(digital_outs);
    if ((digital_outs & B_CLR_DO0) == 0)
        clr_OUT0;
    
	return true;
}


/************************************************************************/
/* REG_EN_LED_CONFS                                                     */
/************************************************************************/
void app_read_REG_EN_LED_CONFS(void) {}
bool app_write_REG_EN_LED_CONFS(void *a)
{
	uint8_t reg = *((uint8_t*)a);
    
    if (reg == 0)
        return true;
        
    uint8_t * which_pointer;
    uint8_t * led_pointer;
    
    for (uint8_t i = 8; i != 0; i--)
    {
        if (reg & (1 << (i-1)))
        {
            which_pointer = ((uint8_t*)(&app_regs.REG_LED_CONF0_WHICH)) + (i-1) * 4;
            led_pointer = ((uint8_t*)(&app_regs.REG_LEDS)) + (*which_pointer) * 3;
            
            for (uint8_t j = 3; j != 0; j--)
            {
                *(led_pointer + j - 1) = *(which_pointer + j - 1 + 1);
            }
        }
    }

    PMIC_CTRL = PMIC_RREN_bm | PMIC_LOLVLEN_bm;
    update_3rgbs(app_regs.REG_LEDS, app_regs.REG_LEDS + 3, app_regs.REG_LEDS + 6);
    PMIC_CTRL = PMIC_RREN_bm | PMIC_LOLVLEN_bm | PMIC_MEDLVLEN_bm | PMIC_HILVLEN_bm;
    
	return true;
}


/************************************************************************/
/* REG_DIS_LED_CONFS                                                    */
/************************************************************************/
void app_read_REG_DIS_LED_CONFS(void) {}
bool app_write_REG_DIS_LED_CONFS(void *a)
{
    uint8_t reg = *((uint8_t*)a);
    
    if (reg == 0)
    return true;
    
    uint8_t * which_pointer;
    uint8_t * led_pointer;
    
    for (uint8_t i = 8; i != 0; i--)
    {
        if (reg & (1 << (i-1)))
        {
            which_pointer = ((uint8_t*)(&app_regs.REG_LED_CONF0_WHICH)) + (i-1) * 4;
            led_pointer = ((uint8_t*)(&app_regs.REG_LEDS)) + (*which_pointer) * 3;
            
            for (uint8_t j = 3; j != 0; j--)
            {
                *(led_pointer + j - 1) = 0;
            }
        }
    }
    
    PMIC_CTRL = PMIC_RREN_bm | PMIC_LOLVLEN_bm;
    update_3rgbs(app_regs.REG_LEDS, app_regs.REG_LEDS + 3, app_regs.REG_LEDS + 6);
    PMIC_CTRL = PMIC_RREN_bm | PMIC_LOLVLEN_bm | PMIC_MEDLVLEN_bm | PMIC_HILVLEN_bm;
    
    return true;
}


/************************************************************************/
/* REG_LEDS                                                             */
/************************************************************************/
// This register is an array with 9 positions
void app_read_REG_LEDS(void) {}
bool app_write_REG_LEDS(void *a)
{
    //for (uint8_t i = 9; i != 0; i--)
    //    app_regs.REG_LEDS[i-1] = *(((uint8_t*)a) + i-1);
    
    uint8_t *pi;
    uint8_t *po;
    uint8_t n;

    pi = app_regs.REG_LEDS;
    po = (uint8_t*)a;
    n=9;
    while(n--){
        *pi++ = *po++;
    }
    
    PMIC_CTRL = PMIC_RREN_bm | PMIC_LOLVLEN_bm;
    update_3rgbs(app_regs.REG_LEDS, app_regs.REG_LEDS + 3, app_regs.REG_LEDS + 6);
    PMIC_CTRL = PMIC_RREN_bm | PMIC_LOLVLEN_bm | PMIC_MEDLVLEN_bm | PMIC_HILVLEN_bm;
    
    return true;
}


/************************************************************************/
/* REG_DOUT5_PULSE                                                      */
/************************************************************************/
void app_read_REG_DOUT4_PULSE(void) {}
bool app_write_REG_DOUT4_PULSE(void *a)
{
	if (*((uint8_t*)a) == 0)
        return false;

	app_regs.REG_DOUT4_PULSE = *((uint8_t*)a);
	return true;
}


/************************************************************************/
/* REG_DOUT5_PULSE                                                      */
/************************************************************************/
void app_read_REG_DOUT5_PULSE(void) {}
bool app_write_REG_DOUT5_PULSE(void *a)
{
	if (*((uint8_t*)a) == 0)
	    return false;

	app_regs.REG_DOUT5_PULSE = *((uint8_t*)a);
	return true;
}


/************************************************************************/
/* REG_RESERVED3                                                        */
/************************************************************************/
void app_read_REG_RESERVED3(void)
{
	//app_regs.REG_RESERVED3 = 0;

}

bool app_write_REG_RESERVED3(void *a)
{
	uint8_t reg = *((uint8_t*)a);

	app_regs.REG_RESERVED3 = reg;
	return true;
}


/************************************************************************/
/* REG_SAMPLE_RATE                                                      */
/************************************************************************/
void app_read_REG_SAMPLE_RATE(void) {}
bool app_write_REG_SAMPLE_RATE(void *a)
{
    if (*((uint8_t*)a) & ~MSK_ACQ_RATE)
    return false;

    app_regs.REG_SAMPLE_RATE = *((uint8_t*)a);
    return true;
}


/************************************************************************/
/* REG_USE_FEEDBACK                                                     */
/************************************************************************/
void app_read_REG_USE_FEEDBACK(void)
{
	//app_regs.REG_USE_FEEDBACK = 0;

}

bool app_write_REG_USE_FEEDBACK(void *a)
{
	uint8_t reg = *((uint8_t*)a);

	app_regs.REG_USE_FEEDBACK = reg;
	return true;
}


/************************************************************************/
/* REG_MOTOR_MICROSTEP                                                  */
/************************************************************************/
void app_read_REG_MOTOR_MICROSTEP(void) {}
bool app_write_REG_MOTOR_MICROSTEP(void *a)
{
	uint8_t reg = *((uint8_t*)a);
    
    if (reg & ~MSK_MICROSTEP)
        return false;
	
    switch (reg)
    {
        case GM_STEP_FULL:
            clr_M_MS1;
            clr_M_MS2;         
            break;
            
        case GM_STEP_HALF:
            set_M_MS1;
            clr_M_MS2;
            break;
            
        case GM_STEP_QUARTER:
            clr_M_MS1;
            set_M_MS2;
            break;
            
        case GM_STEP_EIGHTH:
            set_M_MS1;
            set_M_MS2;
            break;
    }    
    
    app_regs.REG_MOTOR_MICROSTEP = reg;
	return true;
}


/************************************************************************/
/* REG_RESERVED4                                                        */
/************************************************************************/
void app_read_REG_RESERVED4(void)
{
	//app_regs.REG_RESERVED4 = 0;

}

bool app_write_REG_RESERVED4(void *a)
{
	uint8_t reg = *((uint8_t*)a);

	app_regs.REG_RESERVED4 = reg;
	return true;
}


/************************************************************************/
/* REG_RESERVED5                                                        */
/************************************************************************/
void app_read_REG_RESERVED5(void)
{
	//app_regs.REG_RESERVED5 = 0;

}

bool app_write_REG_RESERVED5(void *a)
{
	uint8_t reg = *((uint8_t*)a);

	app_regs.REG_RESERVED5 = reg;
	return true;
}


/************************************************************************/
/* REG_POS_CURRENT                                                      */
/************************************************************************/
void app_read_REG_POS_CURRENT(void) {}
bool app_write_REG_POS_CURRENT(void *a) { return false; }


/************************************************************************/
/* REG_POS_TARGET                                                       */
/************************************************************************/
void app_read_REG_POS_TARGET(void) {}
bool app_write_REG_POS_TARGET(void *a)
{
	uint16_t reg = *((uint16_t*)a);
    
    //if (app_regs.REG_RESET_MOTOR == B_RST_MOTOR)
    //    return true;
    
    if (reg > app_regs.REG_POS_MAXIMUM)
        return false;

	app_regs.REG_POS_TARGET = reg;
	return true;
}


/************************************************************************/
/* REG_POS_0                                                            */
/************************************************************************/
void app_read_REG_POS_0(void) {}
bool app_write_REG_POS_0(void *a)
{
	uint16_t reg = *((uint16_t*)a);
    
    if (reg > app_regs.REG_POS_MAXIMUM)
        return false;

	app_regs.REG_POS_0 = reg;
	return true;
}


/************************************************************************/
/* REG_POS_1                                                            */
/************************************************************************/
void app_read_REG_POS_1(void) {}
bool app_write_REG_POS_1(void *a)
{
	uint16_t reg = *((uint16_t*)a);
	
	if (reg > app_regs.REG_POS_MAXIMUM)
	    return false;

	app_regs.REG_POS_1 = reg;
	return true;
}


/************************************************************************/
/* REG_POS_2                                                            */
/************************************************************************/
void app_read_REG_POS_2(void) {}
bool app_write_REG_POS_2(void *a)
{
	uint16_t reg = *((uint16_t*)a);
	
	if (reg > app_regs.REG_POS_MAXIMUM)
	    return false;

	app_regs.REG_POS_2 = reg;
	return true;
}


/************************************************************************/
/* REG_RESERVED6                                                        */
/************************************************************************/
void app_read_REG_POS_MAXIMUM(void) {}
bool app_write_REG_POS_MAXIMUM(void *a)
{
	uint16_t reg = *((uint16_t*)a);

	app_regs.REG_POS_MAXIMUM = reg;
	return true;
}


/************************************************************************/
/* REG_RESERVED7                                                        */
/************************************************************************/
void app_read_REG_RESERVED7(void) {}
bool app_write_REG_RESERVED7(void *a) { return true; }


/************************************************************************/
/* REG_LED_CONF0_WHICH                                                  */
/************************************************************************/
void app_read_REG_LED_CONF0_WHICH(void) {}
bool app_write_REG_LED_CONF0_WHICH(void *a)
{
	if (*((uint8_t*)a) & (~MSK_LED_CONF))
        return false;

	app_regs.REG_LED_CONF0_WHICH = *((uint8_t*)a);
	return true;
}


/************************************************************************/
/* REG_LED_CONF0_GREEN                                                  */
/************************************************************************/
void app_read_REG_LED_CONF0_GREEN(void) {}
bool app_write_REG_LED_CONF0_GREEN(void *a) { app_regs.REG_LED_CONF0_GREEN = *((uint8_t*)a); return true; }


/************************************************************************/
/* REG_LED_CONF0_RED                                                    */
/************************************************************************/
void app_read_REG_LED_CONF0_RED(void) {}
bool app_write_REG_LED_CONF0_RED(void *a) { app_regs.REG_LED_CONF0_RED = *((uint8_t*)a); return true; }


/************************************************************************/
/* REG_LED_CONF0_BLUE                                                   */
/************************************************************************/
void app_read_REG_LED_CONF0_BLUE(void) {}
bool app_write_REG_LED_CONF0_BLUE(void *a) { app_regs.REG_LED_CONF0_BLUE = *((uint8_t*)a); return true; }


/************************************************************************/
/* REG_LED_CONF1_WHICH                                                  */
/************************************************************************/
void app_read_REG_LED_CONF1_WHICH(void) {}
bool app_write_REG_LED_CONF1_WHICH(void *a)
{
    if (*((uint8_t*)a) & (~MSK_LED_CONF))
        return false;

	app_regs.REG_LED_CONF1_WHICH = *((uint8_t*)a);
	return true;
}


/************************************************************************/
/* REG_LED_CONF1_GREEN                                                  */
/************************************************************************/
void app_read_REG_LED_CONF1_GREEN(void) {}
bool app_write_REG_LED_CONF1_GREEN(void *a) { app_regs.REG_LED_CONF1_GREEN = *((uint8_t*)a); return true; }


/************************************************************************/
/* REG_LED_CONF1_RED                                                    */
/************************************************************************/
void app_read_REG_LED_CONF1_RED(void) {}
bool app_write_REG_LED_CONF1_RED(void *a) { app_regs.REG_LED_CONF1_RED = *((uint8_t*)a); return true; }


/************************************************************************/
/* REG_LED_CONF1_BLUE                                                   */
/************************************************************************/
void app_read_REG_LED_CONF1_BLUE(void) {}
bool app_write_REG_LED_CONF1_BLUE(void *a) { app_regs.REG_LED_CONF1_BLUE = *((uint8_t*)a); return true; }


/************************************************************************/
/* REG_LED_CONF2_WHICH                                                  */
/************************************************************************/
void app_read_REG_LED_CONF2_WHICH(void) {}
bool app_write_REG_LED_CONF2_WHICH(void *a)
{
    if (*((uint8_t*)a) & (~MSK_LED_CONF))
        return false;

	app_regs.REG_LED_CONF2_WHICH = *((uint8_t*)a);
	return true;
}


/************************************************************************/
/* REG_LED_CONF2_GREEN                                                  */
/************************************************************************/
void app_read_REG_LED_CONF2_GREEN(void) {}
bool app_write_REG_LED_CONF2_GREEN(void *a) { app_regs.REG_LED_CONF2_GREEN = *((uint8_t*)a); return true; }


/************************************************************************/
/* REG_LED_CONF2_RED                                                    */
/************************************************************************/
void app_read_REG_LED_CONF2_RED(void) {}
bool app_write_REG_LED_CONF2_RED(void *a) { app_regs.REG_LED_CONF2_RED = *((uint8_t*)a); return true; }


/************************************************************************/
/* REG_LED_CONF2_BLUE                                                   */
/************************************************************************/
void app_read_REG_LED_CONF2_BLUE(void) {}
bool app_write_REG_LED_CONF2_BLUE(void *a) { app_regs.REG_LED_CONF2_BLUE = *((uint8_t*)a); return true; }


/************************************************************************/
/* REG_LED_CONF3_WHICH                                                  */
/************************************************************************/
void app_read_REG_LED_CONF3_WHICH(void) {}
bool app_write_REG_LED_CONF3_WHICH(void *a)
{
    if (*((uint8_t*)a) & (~MSK_LED_CONF))
        return false;

	app_regs.REG_LED_CONF3_WHICH = *((uint8_t*)a);
	return true;
}


/************************************************************************/
/* REG_LED_CONF3_GREEN                                                  */
/************************************************************************/
void app_read_REG_LED_CONF3_GREEN(void) {}
bool app_write_REG_LED_CONF3_GREEN(void *a) { app_regs.REG_LED_CONF3_GREEN = *((uint8_t*)a); return true; }
    

/************************************************************************/
/* REG_LED_CONF3_RED                                                    */
/************************************************************************/
void app_read_REG_LED_CONF3_RED(void) {}
bool app_write_REG_LED_CONF3_RED(void *a) { app_regs.REG_LED_CONF3_RED = *((uint8_t*)a); return true; }


/************************************************************************/
/* REG_LED_CONF3_BLUE                                                   */
/************************************************************************/
void app_read_REG_LED_CONF3_BLUE(void) {}
bool app_write_REG_LED_CONF3_BLUE(void *a) { app_regs.REG_LED_CONF3_BLUE = *((uint8_t*)a); return true; }


/************************************************************************/
/* REG_LED_CONF4_WHICH                                                  */
/************************************************************************/
void app_read_REG_LED_CONF4_WHICH(void) {}
bool app_write_REG_LED_CONF4_WHICH(void *a)
{	
	if (*((uint8_t*)a) & (~MSK_LED_CONF))
	    return false;

	app_regs.REG_LED_CONF4_WHICH = *((uint8_t*)a);
	return true;
}


/************************************************************************/
/* REG_LED_CONF4_GREEN                                                  */
/************************************************************************/
void app_read_REG_LED_CONF4_GREEN(void) {}
bool app_write_REG_LED_CONF4_GREEN(void *a) { app_regs.REG_LED_CONF4_GREEN = *((uint8_t*)a); return true; }


/************************************************************************/
/* REG_LED_CONF4_RED                                                    */
/************************************************************************/
void app_read_REG_LED_CONF4_RED(void) {}
bool app_write_REG_LED_CONF4_RED(void *a) { app_regs.REG_LED_CONF4_RED = *((uint8_t*)a); return true; }


/************************************************************************/
/* REG_LED_CONF4_BLUE                                                   */
/************************************************************************/
void app_read_REG_LED_CONF4_BLUE(void) {}
bool app_write_REG_LED_CONF4_BLUE(void *a) { app_regs.REG_LED_CONF4_BLUE = *((uint8_t*)a); return true; }


/************************************************************************/
/* REG_LED_CONF5_WHICH                                                  */
/************************************************************************/
void app_read_REG_LED_CONF5_WHICH(void) {}
bool app_write_REG_LED_CONF5_WHICH(void *a)
{
    if (*((uint8_t*)a) & (~MSK_LED_CONF))
        return false;

	app_regs.REG_LED_CONF5_WHICH = *((uint8_t*)a);
	return true;
}


/************************************************************************/
/* REG_LED_CONF5_GREEN                                                  */
/************************************************************************/
void app_read_REG_LED_CONF5_GREEN(void) {}
bool app_write_REG_LED_CONF5_GREEN(void *a) { app_regs.REG_LED_CONF5_GREEN = *((uint8_t*)a); return true; }


/************************************************************************/
/* REG_LED_CONF5_RED                                                    */
/************************************************************************/
void app_read_REG_LED_CONF5_RED(void) {}
bool app_write_REG_LED_CONF5_RED(void *a) { app_regs.REG_LED_CONF5_RED = *((uint8_t*)a); return true; }


/************************************************************************/
/* REG_LED_CONF5_BLUE                                                   */
/************************************************************************/
void app_read_REG_LED_CONF5_BLUE(void) {}
bool app_write_REG_LED_CONF5_BLUE(void *a) { app_regs.REG_LED_CONF5_BLUE = *((uint8_t*)a); return true; }


/************************************************************************/
/* REG_LED_CONF6_WHICH                                                  */
/************************************************************************/
void app_read_REG_LED_CONF6_WHICH(void) {}
bool app_write_REG_LED_CONF6_WHICH(void *a)
{
    if (*((uint8_t*)a) & (~MSK_LED_CONF))
        return false;

	app_regs.REG_LED_CONF6_WHICH = *((uint8_t*)a);
	return true;
}


/************************************************************************/
/* REG_LED_CONF6_GREEN                                                  */
/************************************************************************/
void app_read_REG_LED_CONF6_GREEN(void) {}
bool app_write_REG_LED_CONF6_GREEN(void *a) { app_regs.REG_LED_CONF6_GREEN = *((uint8_t*)a); return true; }


/************************************************************************/
/* REG_LED_CONF6_RED                                                    */
/************************************************************************/
void app_read_REG_LED_CONF6_RED(void) {}
bool app_write_REG_LED_CONF6_RED(void *a) { app_regs.REG_LED_CONF6_RED = *((uint8_t*)a); return true; }


/************************************************************************/
/* REG_LED_CONF6_BLUE                                                   */
/************************************************************************/
void app_read_REG_LED_CONF6_BLUE(void) {}
bool app_write_REG_LED_CONF6_BLUE(void *a) { app_regs.REG_LED_CONF6_BLUE = *((uint8_t*)a); return true; }


/************************************************************************/
/* REG_LED_CONF7_WHICH                                                  */
/************************************************************************/
void app_read_REG_LED_CONF7_WHICH(void) {}
bool app_write_REG_LED_CONF7_WHICH(void *a)
{
    if (*((uint8_t*)a) & (~MSK_LED_CONF))
        return false;

	app_regs.REG_LED_CONF7_WHICH = *((uint8_t*)a);
	return true;
}


/************************************************************************/
/* REG_LED_CONF7_GREEN                                                  */
/************************************************************************/
void app_read_REG_LED_CONF7_GREEN(void) {}
bool app_write_REG_LED_CONF7_GREEN(void *a) { app_regs.REG_LED_CONF7_GREEN = *((uint8_t*)a); return true; }


/************************************************************************/
/* REG_LED_CONF7_RED                                                    */
/************************************************************************/
void app_read_REG_LED_CONF7_RED(void) {}
bool app_write_REG_LED_CONF7_RED(void *a) { app_regs.REG_LED_CONF7_RED = *((uint8_t*)a); return true; }


/************************************************************************/
/* REG_LED_CONF7_BLUE                                                   */
/************************************************************************/
void app_read_REG_LED_CONF7_BLUE(void) {}
bool app_write_REG_LED_CONF7_BLUE(void *a) { app_regs.REG_LED_CONF7_BLUE = *((uint8_t*)a); return true; }


/************************************************************************/
/* REG_RESERVED8                                                        */
/************************************************************************/
void app_read_REG_RESERVED8(void) {}
bool app_write_REG_RESERVED8(void *a) { return true; }


/************************************************************************/
/* REG_RESERVED9                                                        */
/************************************************************************/
void app_read_REG_RESERVED9(void) {}
bool app_write_REG_RESERVED9(void *a) { return true; }


/************************************************************************/
/* REG_TH_STANDBY                                                       */
/************************************************************************/
void app_read_REG_TH_STANDBY(void) {}
bool app_write_REG_TH_STANDBY(void *a)
{
	if (*((uint16_t*)a) == 0)
	return false;

	app_regs.REG_TH_STANDBY = *((uint16_t*)a);
	return true;
}


/************************************************************************/
/* REG_TH_0                                                             */
/************************************************************************/
void app_read_REG_TH_0(void) {}
bool app_write_REG_TH_0(void *a)
{
	if (*((uint16_t*)a) == 0)
	    return false;

	app_regs.REG_TH_0 = *((uint16_t*)a);
	return true;
}


/************************************************************************/
/* REG_TH_1                                                             */
/************************************************************************/
void app_read_REG_TH_1(void) {}
bool app_write_REG_TH_1(void *a)
{
	if (*((uint16_t*)a) == 0)
	return false;

	app_regs.REG_TH_1 = *((uint16_t*)a);
	return true;
}


/************************************************************************/
/* REG_TH_2                                                             */
/************************************************************************/
void app_read_REG_TH_2(void) {}
bool app_write_REG_TH_2(void *a)
{
	if (*((uint16_t*)a) == 0)
	return false;

	app_regs.REG_TH_2 = *((uint16_t*)a);
	return true;
}


/************************************************************************/
/* REG_TH_3                                                             */
/************************************************************************/
void app_read_REG_TH_3(void) {}
bool app_write_REG_TH_3(void *a)
{
	if (*((uint16_t*)a) == 0)
	    return false;

	app_regs.REG_TH_3 = *((uint16_t*)a);
	return true;
}


/************************************************************************/
/* REG_TH_STANDBY_MS                                                    */
/************************************************************************/
void app_read_REG_TH_STANDBY_MS(void) {}
bool app_write_REG_TH_STANDBY_MS(void *a)
{
    if ((*((uint8_t*)a) == 0) || (*((uint8_t*)a) > 200))
        return false;

	app_regs.REG_TH_STANDBY_MS = *((uint8_t*)a);
	return true;
}


/************************************************************************/
/* REG_TH_0_ON                                                          */
/************************************************************************/
void app_read_REG_TH_0_ON(void) {}
bool app_write_REG_TH_0_ON(void *a)
{
    if ((*((uint16_t*)a) == 0) || (*((uint8_t*)a) > 60000))
        return false;

	app_regs.REG_TH_0_ON = *((uint16_t*)a);
	return true;
}


/************************************************************************/
/* REG_TH_0_OFF                                                         */
/************************************************************************/
void app_read_REG_TH_0_OFF(void) {}
bool app_write_REG_TH_0_OFF(void *a)
{
    if ((*((uint16_t*)a) == 0) || (*((uint8_t*)a) > 60000))
        return false;

	app_regs.REG_TH_0_OFF = *((uint16_t*)a);
	return true;
}


/************************************************************************/
/* REG_TH_1_ON                                                          */
/************************************************************************/
void app_read_REG_TH_1_ON(void) {}
bool app_write_REG_TH_1_ON(void *a)
{
    if ((*((uint16_t*)a) == 0) || (*((uint8_t*)a) > 60000))
        return false;

	app_regs.REG_TH_1_ON = *((uint16_t*)a);
	return true;
}


/************************************************************************/
/* REG_TH_1_OFF                                                         */
/************************************************************************/
void app_read_REG_TH_1_OFF(void) {}
bool app_write_REG_TH_1_OFF(void *a)
{
    if ((*((uint16_t*)a) == 0) || (*((uint8_t*)a) > 60000))
        return false;

	app_regs.REG_TH_1_OFF = *((uint16_t*)a);
	return true;
}


/************************************************************************/
/* REG_TH_2_ON                                                          */
/************************************************************************/
void app_read_REG_TH_2_ON(void) {}
bool app_write_REG_TH_2_ON(void *a)
{
    if ((*((uint16_t*)a) == 0) || (*((uint8_t*)a) > 60000))
        return false;

	app_regs.REG_TH_2_ON = *((uint16_t*)a);
	return true;
}


/************************************************************************/
/* REG_TH_2_OFF                                                         */
/************************************************************************/
void app_read_REG_TH_2_OFF(void) {}
bool app_write_REG_TH_2_OFF(void *a)
{
    if ((*((uint16_t*)a) == 0) || (*((uint8_t*)a) > 60000))
        return false;

	app_regs.REG_TH_2_OFF = *((uint16_t*)a);
	return true;
}


/************************************************************************/
/* REG_TH_3_ON                                                          */
/************************************************************************/
void app_read_REG_TH_3_ON(void) {}
bool app_write_REG_TH_3_ON(void *a)
{
    if ((*((uint16_t*)a) == 0) || (*((uint8_t*)a) > 60000))
        return false;

	app_regs.REG_TH_3_ON = *((uint16_t*)a);
	return true;
}


/************************************************************************/
/* REG_TH_3_OFF                                                         */
/************************************************************************/
void app_read_REG_TH_3_OFF(void) {}
bool app_write_REG_TH_3_OFF(void *a)
{
    if ((*((uint16_t*)a) == 0) || (*((uint8_t*)a) > 60000))
        return false;

	app_regs.REG_TH_3_OFF = *((uint16_t*)a);
	return true;
}


/************************************************************************/
/* REG_TH_0_LED_CONF                                                    */
/************************************************************************/
void app_read_REG_TH_0_LED_CONF(void) {}
bool app_write_REG_TH_0_LED_CONF(void *a)
{
	if (*((uint8_t*)a) & ~(MSK_TH_LED_CONF | B_CLR_THE_OTHER_LEDS | B_CLR_TH0_LED_CONF))
        return false;

	app_regs.REG_TH_0_LED_CONF = *((uint8_t*)a);
	return true;
}


/************************************************************************/
/* REG_TH_1_LED_CONF                                                    */
/************************************************************************/
void app_read_REG_TH_1_LED_CONF(void) {}
bool app_write_REG_TH_1_LED_CONF(void *a)
{
    if (*((uint8_t*)a) & ~(MSK_TH_LED_CONF | B_CLR_THE_OTHER_LEDS))
        return false;

	app_regs.REG_TH_1_LED_CONF = *((uint8_t*)a);
	return true;
}


/************************************************************************/
/* REG_TH_2_LED_CONF                                                    */
/************************************************************************/
void app_read_REG_TH_2_LED_CONF(void) {}
bool app_write_REG_TH_2_LED_CONF(void *a)
{
	if (*((uint8_t*)a) & ~(MSK_TH_LED_CONF | B_CLR_THE_OTHER_LEDS))
	    return false;

	app_regs.REG_TH_2_LED_CONF = *((uint8_t*)a);
	return true;
}


/************************************************************************/
/* REG_TH_3_LED_CONF                                                    */
/************************************************************************/
void app_read_REG_TH_3_LED_CONF(void) {}
bool app_write_REG_TH_3_LED_CONF(void *a)
{
    if (*((uint8_t*)a) & ~(MSK_TH_LED_CONF | B_CLR_THE_OTHER_LEDS))
        return false;

    app_regs.REG_TH_3_LED_CONF = *((uint8_t*)a);
    return true;
}


/************************************************************************/
/* REG_RESERVED10                                                       */
/************************************************************************/
void app_read_REG_RESERVED10(void) {}
bool app_write_REG_RESERVED10(void *a) { return true; }


/************************************************************************/
/* REG_RESERVED11                                                       */
/************************************************************************/
void app_read_REG_RESERVED11(void) {}
bool app_write_REG_RESERVED11(void *a) { return true; }


/************************************************************************/
/* REG_CONF_DI0                                                         */
/************************************************************************/
void app_read_REG_CONF_DI0(void)
{
	//app_regs.REG_CONF_DI0 = 0;

}

bool app_write_REG_CONF_DI0(void *a)
{
	uint8_t reg = *((uint8_t*)a);

	app_regs.REG_CONF_DI0 = reg;
	return true;
}


/************************************************************************/
/* REG_CONF_DI1                                                         */
/************************************************************************/
void app_read_REG_CONF_DI1(void)
{
	//app_regs.REG_CONF_DI1 = 0;

}

bool app_write_REG_CONF_DI1(void *a)
{
	uint8_t reg = *((uint8_t*)a);

	app_regs.REG_CONF_DI1 = reg;
	return true;
}


/************************************************************************/
/* REG_CONF_DI2                                                         */
/************************************************************************/
void app_read_REG_CONF_DI2(void)
{
	//app_regs.REG_CONF_DI2 = 0;

}

bool app_write_REG_CONF_DI2(void *a)
{
	uint8_t reg = *((uint8_t*)a);

	app_regs.REG_CONF_DI2 = reg;
	return true;
}


/************************************************************************/
/* REG_CONF_DI3                                                         */
/************************************************************************/
void app_read_REG_CONF_DI3(void)
{
	//app_regs.REG_CONF_DI3 = 0;

}

bool app_write_REG_CONF_DI3(void *a)
{
	uint8_t reg = *((uint8_t*)a);

	app_regs.REG_CONF_DI3 = reg;
	return true;
}


/************************************************************************/
/* REG_RESERVED12                                                       */
/************************************************************************/
void app_read_REG_RESERVED12(void)
{
	//app_regs.REG_RESERVED12 = 0;

}

bool app_write_REG_RESERVED12(void *a)
{
	uint8_t reg = *((uint8_t*)a);

	app_regs.REG_RESERVED12 = reg;
	return true;
}


/************************************************************************/
/* REG_RESERVED13                                                       */
/************************************************************************/
void app_read_REG_RESERVED13(void)
{
	//app_regs.REG_RESERVED13 = 0;

}

bool app_write_REG_RESERVED13(void *a)
{
	uint8_t reg = *((uint8_t*)a);

	app_regs.REG_RESERVED13 = reg;
	return true;
}


/************************************************************************/
/* REG_CONF_DO0                                                         */
/************************************************************************/
void app_read_REG_CONF_DO0(void)
{
	//app_regs.REG_CONF_DO0 = 0;

}

bool app_write_REG_CONF_DO0(void *a)
{
	uint8_t reg = *((uint8_t*)a);

	app_regs.REG_CONF_DO0 = reg;
	return true;
}


/************************************************************************/
/* REG_CONF_DO1                                                         */
/************************************************************************/
void app_read_REG_CONF_DO1(void)
{
	//app_regs.REG_CONF_DO1 = 0;

}

bool app_write_REG_CONF_DO1(void *a)
{
	uint8_t reg = *((uint8_t*)a);

	app_regs.REG_CONF_DO1 = reg;
	return true;
}


/************************************************************************/
/* REG_CONF_DO2                                                         */
/************************************************************************/
void app_read_REG_CONF_DO2(void)
{
	//app_regs.REG_CONF_DO2 = 0;

}

bool app_write_REG_CONF_DO2(void *a)
{
	uint8_t reg = *((uint8_t*)a);

	app_regs.REG_CONF_DO2 = reg;
	return true;
}


/************************************************************************/
/* REG_CONF_DO3                                                         */
/************************************************************************/
void app_read_REG_CONF_DO3(void)
{
	//app_regs.REG_CONF_DO3 = 0;

}

bool app_write_REG_CONF_DO3(void *a)
{
	uint8_t reg = *((uint8_t*)a);

	app_regs.REG_CONF_DO3 = reg;
	return true;
}


/************************************************************************/
/* REG_CONF_DO4                                                         */
/************************************************************************/
void app_read_REG_CONF_DO4(void)
{
	//app_regs.REG_CONF_DO4 = 0;

}

bool app_write_REG_CONF_DO4(void *a)
{
	uint8_t reg = *((uint8_t*)a);

	app_regs.REG_CONF_DO4 = reg;
	return true;
}


/************************************************************************/
/* REG_CONF_DO5                                                         */
/************************************************************************/
void app_read_REG_CONF_DO5(void)
{
	//app_regs.REG_CONF_DO5 = 0;

}

bool app_write_REG_CONF_DO5(void *a)
{
	uint8_t reg = *((uint8_t*)a);

	app_regs.REG_CONF_DO5 = reg;
	return true;
}


/************************************************************************/
/* REG_RESERVED14                                                       */
/************************************************************************/
void app_read_REG_RESERVED14(void)
{
	//app_regs.REG_RESERVED14 = 0;

}

bool app_write_REG_RESERVED14(void *a)
{
	uint8_t reg = *((uint8_t*)a);

	app_regs.REG_RESERVED14 = reg;
	return true;
}


/************************************************************************/
/* REG_RESERVED15                                                       */
/************************************************************************/
void app_read_REG_RESERVED15(void)
{
	//app_regs.REG_RESERVED15 = 0;

}

bool app_write_REG_RESERVED15(void *a)
{
	uint8_t reg = *((uint8_t*)a);

	app_regs.REG_RESERVED15 = reg;
	return true;
}


/************************************************************************/
/* REG_EVNT_ENABLE                                                      */
/************************************************************************/
void app_read_REG_EVNT_ENABLE(void) {}
bool app_write_REG_EVNT_ENABLE(void *a)
{
	uint8_t reg = *((uint8_t*)a);

	app_regs.REG_EVNT_ENABLE = reg;
	return true;
}