#ifndef _APP_FUNCTIONS_H_
#define _APP_FUNCTIONS_H_
#include <avr/io.h>


/************************************************************************/
/* Define if not defined                                                */
/************************************************************************/
#ifndef bool
	#define bool uint8_t
#endif
#ifndef true
	#define true 1
#endif
#ifndef false
	#define false 0
#endif


/************************************************************************/
/* Prototypes                                                           */
/************************************************************************/
void app_read_REG_THRESHOLDS(void);
void app_read_REG_DATA(void);
void app_read_REG_INPUTS(void);
void app_read_REG_RESERVED0(void);
void app_read_REG_RESERVED1(void);
void app_read_REG_FEEDBACK(void);
void app_read_REG_RESERVED2(void);
void app_read_REG_RESET_ANGLE(void);
void app_read_REG_RESET_MOTOR(void);
void app_read_REG_HIDE_LEVER(void);
void app_read_REG_SET_DOUTS(void);
void app_read_REG_CLEAR_DOUTS(void);
void app_read_REG_EN_LED_CONFS(void);
void app_read_REG_DIS_LED_CONFS(void);
void app_read_REG_LEDS(void);
void app_read_REG_DOUT4_PULSE(void);
void app_read_REG_DOUT5_PULSE(void);
void app_read_REG_RESERVED3(void);
void app_read_REG_SAMPLE_RATE(void);
void app_read_REG_USE_FEEDBACK(void);
void app_read_REG_MOTOR_MICROSTEP(void);
void app_read_REG_RESERVED4(void);
void app_read_REG_RESERVED5(void);
void app_read_REG_POS_CURRENT(void);
void app_read_REG_POS_TARGET(void);
void app_read_REG_POS_0(void);
void app_read_REG_POS_1(void);
void app_read_REG_POS_2(void);
void app_read_REG_POS_MAXIMUM(void);
void app_read_REG_RESERVED7(void);
void app_read_REG_LED_CONF0_WHICH(void);
void app_read_REG_LED_CONF0_GREEN(void);
void app_read_REG_LED_CONF0_RED(void);
void app_read_REG_LED_CONF0_BLUE(void);
void app_read_REG_LED_CONF1_WHICH(void);
void app_read_REG_LED_CONF1_GREEN(void);
void app_read_REG_LED_CONF1_RED(void);
void app_read_REG_LED_CONF1_BLUE(void);
void app_read_REG_LED_CONF2_WHICH(void);
void app_read_REG_LED_CONF2_GREEN(void);
void app_read_REG_LED_CONF2_RED(void);
void app_read_REG_LED_CONF2_BLUE(void);
void app_read_REG_LED_CONF3_WHICH(void);
void app_read_REG_LED_CONF3_GREEN(void);
void app_read_REG_LED_CONF3_RED(void);
void app_read_REG_LED_CONF3_BLUE(void);
void app_read_REG_LED_CONF4_WHICH(void);
void app_read_REG_LED_CONF4_GREEN(void);
void app_read_REG_LED_CONF4_RED(void);
void app_read_REG_LED_CONF4_BLUE(void);
void app_read_REG_LED_CONF5_WHICH(void);
void app_read_REG_LED_CONF5_GREEN(void);
void app_read_REG_LED_CONF5_RED(void);
void app_read_REG_LED_CONF5_BLUE(void);
void app_read_REG_LED_CONF6_WHICH(void);
void app_read_REG_LED_CONF6_GREEN(void);
void app_read_REG_LED_CONF6_RED(void);
void app_read_REG_LED_CONF6_BLUE(void);
void app_read_REG_LED_CONF7_WHICH(void);
void app_read_REG_LED_CONF7_GREEN(void);
void app_read_REG_LED_CONF7_RED(void);
void app_read_REG_LED_CONF7_BLUE(void);
void app_read_REG_RESERVED8(void);
void app_read_REG_RESERVED9(void);
void app_read_REG_TH_STANDBY(void);
void app_read_REG_TH_0(void);
void app_read_REG_TH_1(void);
void app_read_REG_TH_2(void);
void app_read_REG_TH_3(void);
void app_read_REG_TH_STANDBY_MS(void);
void app_read_REG_TH_0_ON(void);
void app_read_REG_TH_0_OFF(void);
void app_read_REG_TH_1_ON(void);
void app_read_REG_TH_1_OFF(void);
void app_read_REG_TH_2_ON(void);
void app_read_REG_TH_2_OFF(void);
void app_read_REG_TH_3_ON(void);
void app_read_REG_TH_3_OFF(void);
void app_read_REG_TH_0_LED_CONF(void);
void app_read_REG_TH_1_LED_CONF(void);
void app_read_REG_TH_2_LED_CONF(void);
void app_read_REG_TH_3_LED_CONF(void);
void app_read_REG_RESERVED10(void);
void app_read_REG_RESERVED11(void);
void app_read_REG_CONF_DI0(void);
void app_read_REG_CONF_DI1(void);
void app_read_REG_CONF_DI2(void);
void app_read_REG_CONF_DI3(void);
void app_read_REG_RESERVED12(void);
void app_read_REG_RESERVED13(void);
void app_read_REG_CONF_DO0(void);
void app_read_REG_CONF_DO1(void);
void app_read_REG_CONF_DO2(void);
void app_read_REG_CONF_DO3(void);
void app_read_REG_CONF_DO4(void);
void app_read_REG_CONF_DO5(void);
void app_read_REG_RESERVED14(void);
void app_read_REG_RESERVED15(void);
void app_read_REG_EVNT_ENABLE(void);

bool app_write_REG_THRESHOLDS(void *a);
bool app_write_REG_DATA(void *a);
bool app_write_REG_INPUTS(void *a);
bool app_write_REG_RESERVED0(void *a);
bool app_write_REG_RESERVED1(void *a);
bool app_write_REG_FEEDBACK(void *a);
bool app_write_REG_RESERVED2(void *a);
bool app_write_REG_RESET_ANGLE(void *a);
bool app_write_REG_RESET_MOTOR(void *a);
bool app_write_REG_HIDE_LEVER(void *a);
bool app_write_REG_SET_DOUTS(void *a);
bool app_write_REG_CLEAR_DOUTS(void *a);
bool app_write_REG_EN_LED_CONFS(void *a);
bool app_write_REG_DIS_LED_CONFS(void *a);
bool app_write_REG_LEDS(void *a);
bool app_write_REG_DOUT4_PULSE(void *a);
bool app_write_REG_DOUT5_PULSE(void *a);
bool app_write_REG_RESERVED3(void *a);
bool app_write_REG_SAMPLE_RATE(void *a);
bool app_write_REG_USE_FEEDBACK(void *a);
bool app_write_REG_MOTOR_MICROSTEP(void *a);
bool app_write_REG_RESERVED4(void *a);
bool app_write_REG_RESERVED5(void *a);
bool app_write_REG_POS_CURRENT(void *a);
bool app_write_REG_POS_TARGET(void *a);
bool app_write_REG_POS_0(void *a);
bool app_write_REG_POS_1(void *a);
bool app_write_REG_POS_2(void *a);
bool app_write_REG_POS_MAXIMUM(void *a);
bool app_write_REG_RESERVED7(void *a);
bool app_write_REG_LED_CONF0_WHICH(void *a);
bool app_write_REG_LED_CONF0_GREEN(void *a);
bool app_write_REG_LED_CONF0_RED(void *a);
bool app_write_REG_LED_CONF0_BLUE(void *a);
bool app_write_REG_LED_CONF1_WHICH(void *a);
bool app_write_REG_LED_CONF1_GREEN(void *a);
bool app_write_REG_LED_CONF1_RED(void *a);
bool app_write_REG_LED_CONF1_BLUE(void *a);
bool app_write_REG_LED_CONF2_WHICH(void *a);
bool app_write_REG_LED_CONF2_GREEN(void *a);
bool app_write_REG_LED_CONF2_RED(void *a);
bool app_write_REG_LED_CONF2_BLUE(void *a);
bool app_write_REG_LED_CONF3_WHICH(void *a);
bool app_write_REG_LED_CONF3_GREEN(void *a);
bool app_write_REG_LED_CONF3_RED(void *a);
bool app_write_REG_LED_CONF3_BLUE(void *a);
bool app_write_REG_LED_CONF4_WHICH(void *a);
bool app_write_REG_LED_CONF4_GREEN(void *a);
bool app_write_REG_LED_CONF4_RED(void *a);
bool app_write_REG_LED_CONF4_BLUE(void *a);
bool app_write_REG_LED_CONF5_WHICH(void *a);
bool app_write_REG_LED_CONF5_GREEN(void *a);
bool app_write_REG_LED_CONF5_RED(void *a);
bool app_write_REG_LED_CONF5_BLUE(void *a);
bool app_write_REG_LED_CONF6_WHICH(void *a);
bool app_write_REG_LED_CONF6_GREEN(void *a);
bool app_write_REG_LED_CONF6_RED(void *a);
bool app_write_REG_LED_CONF6_BLUE(void *a);
bool app_write_REG_LED_CONF7_WHICH(void *a);
bool app_write_REG_LED_CONF7_GREEN(void *a);
bool app_write_REG_LED_CONF7_RED(void *a);
bool app_write_REG_LED_CONF7_BLUE(void *a);
bool app_write_REG_RESERVED8(void *a);
bool app_write_REG_RESERVED9(void *a);
bool app_write_REG_TH_STANDBY(void *a);
bool app_write_REG_TH_0(void *a);
bool app_write_REG_TH_1(void *a);
bool app_write_REG_TH_2(void *a);
bool app_write_REG_TH_3(void *a);
bool app_write_REG_TH_STANDBY_MS(void *a);
bool app_write_REG_TH_0_ON(void *a);
bool app_write_REG_TH_0_OFF(void *a);
bool app_write_REG_TH_1_ON(void *a);
bool app_write_REG_TH_1_OFF(void *a);
bool app_write_REG_TH_2_ON(void *a);
bool app_write_REG_TH_2_OFF(void *a);
bool app_write_REG_TH_3_ON(void *a);
bool app_write_REG_TH_3_OFF(void *a);
bool app_write_REG_TH_0_LED_CONF(void *a);
bool app_write_REG_TH_1_LED_CONF(void *a);
bool app_write_REG_TH_2_LED_CONF(void *a);
bool app_write_REG_TH_3_LED_CONF(void *a);
bool app_write_REG_RESERVED10(void *a);
bool app_write_REG_RESERVED11(void *a);
bool app_write_REG_CONF_DI0(void *a);
bool app_write_REG_CONF_DI1(void *a);
bool app_write_REG_CONF_DI2(void *a);
bool app_write_REG_CONF_DI3(void *a);
bool app_write_REG_RESERVED12(void *a);
bool app_write_REG_RESERVED13(void *a);
bool app_write_REG_CONF_DO0(void *a);
bool app_write_REG_CONF_DO1(void *a);
bool app_write_REG_CONF_DO2(void *a);
bool app_write_REG_CONF_DO3(void *a);
bool app_write_REG_CONF_DO4(void *a);
bool app_write_REG_CONF_DO5(void *a);
bool app_write_REG_RESERVED14(void *a);
bool app_write_REG_RESERVED15(void *a);
bool app_write_REG_EVNT_ENABLE(void *a);


#endif /* _APP_FUNCTIONS_H_ */