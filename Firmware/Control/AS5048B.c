#include "AS5048B.h"
#include "i2c.h"

static i2c_dev_t encoder;

void initialize_rotary_encoder (uint8_t encoder_address)
{
    encoder.add = encoder_address;    
    i2c0_init();
}

void reset_angle (void)
{
    encoder.reg = REG_OTP_ZERO_POSITION;
    encoder.reg_val = 0;    
    i2c0_wReg(&encoder);
    encoder.reg = REG_OTP_ZERO_POSITION + 1;
    i2c0_wReg(&encoder);
    
    encoder.reg = REG_ANGLE;
    i2c0_rReg(&encoder, 2);
    
    encoder.reg = REG_OTP_ZERO_POSITION;
    encoder.reg_val = encoder.data[0];
    i2c0_wReg(&encoder);
    encoder.reg = REG_OTP_ZERO_POSITION + 1;
    encoder.reg_val = encoder.data[1];
    i2c0_wReg(&encoder);
}

int16_t read_angle (void)
{
    int16_t angle;
    
    encoder.reg = REG_ANGLE;
    i2c0_rReg(&encoder, 2);
    
    angle = encoder.data[0];
    angle = angle << 6;
    angle |= (encoder.data[1] & 0x3F);
    
    return angle;
}