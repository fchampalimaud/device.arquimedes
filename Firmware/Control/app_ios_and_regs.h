#ifndef _APP_IOS_AND_REGS_H_
#define _APP_IOS_AND_REGS_H_
#include "cpu.h"

void init_ios(void);
/************************************************************************/
/* Definition of input pins                                             */
/************************************************************************/
// IN0                    Description: Input 0

#define get_IN0 read_io(PORTD, 4)               // IN0

/************************************************************************/
/* Definition of output pins                                            */
/************************************************************************/
// M_SLEEP                Description: Motor low power mode
// M_DIR                  Description: Motor direction
// M_MS1                  Description: Motor step configuration
// M_RESET                Description: Motor reset
// M_ENABLE               Description: Motor enable
// M_MS2                  Description: Motor step configuration
// OUT0                   Description: Output 0
// LEDS                   Description: One wire LEDs
// SERVO_PWM              Description: PWM of servo motor
// M_STEP                 Description: Motor step
// UCUC                   Description: Trigger the other uC to send the inputs state

/* M_SLEEP */
#define set_M_SLEEP clear_io(PORTA, 0)
#define clr_M_SLEEP set_io(PORTA, 0)
#define tgl_M_SLEEP toggle_io(PORTA, 0)
#define get_M_SLEEP read_io(PORTA, 0)

/* M_DIR */
#define set_M_DIR set_io(PORTA, 2)
#define clr_M_DIR clear_io(PORTA, 2)
#define tgl_M_DIR toggle_io(PORTA, 2)
#define get_M_DIR read_io(PORTA, 2)

/* M_MS1 */
#define set_M_MS1 set_io(PORTA, 3)
#define clr_M_MS1 clear_io(PORTA, 3)
#define tgl_M_MS1 toggle_io(PORTA, 3)
#define get_M_MS1 read_io(PORTA, 3)

/* M_RESET */
#define set_M_RESET clear_io(PORTA, 4)
#define clr_M_RESET set_io(PORTA, 4)
#define tgl_M_RESET toggle_io(PORTA, 4)
#define get_M_RESET read_io(PORTA, 4)

/* M_ENABLE */
#define set_M_ENABLE clear_io(PORTA, 5)
#define clr_M_ENABLE set_io(PORTA, 5)
#define tgl_M_ENABLE toggle_io(PORTA, 5)
#define get_M_ENABLE read_io(PORTA, 5)

/* M_MS2 */
#define set_M_MS2 set_io(PORTA, 6)
#define clr_M_MS2 clear_io(PORTA, 6)
#define tgl_M_MS2 toggle_io(PORTA, 6)
#define get_M_MS2 read_io(PORTA, 6)

/* OUT0 */
#define set_OUT0 set_io(PORTB, 0)
#define clr_OUT0 clear_io(PORTB, 0)
#define tgl_OUT0 toggle_io(PORTB, 0)
#define get_OUT0 read_io(PORTB, 0)

/* LEDS */
#define set_LEDS set_io(PORTC, 7)
#define clr_LEDS clear_io(PORTC, 7)
#define tgl_LEDS toggle_io(PORTC, 7)
#define get_LEDS read_io(PORTC, 7)

/* SERVO_PWM */
#define set_SERVO_PWM set_io(PORTD, 0)
#define clr_SERVO_PWM clear_io(PORTD, 0)
#define tgl_SERVO_PWM toggle_io(PORTD, 0)
#define get_SERVO_PWM read_io(PORTD, 0)

/* M_STEP */
#define set_M_STEP set_io(PORTD, 1)
#define clr_M_STEP clear_io(PORTD, 1)
#define tgl_M_STEP toggle_io(PORTD, 1)
#define get_M_STEP read_io(PORTD, 1)

/* UCUC */
#define set_UCUC set_io(PORTD, 5)
#define clr_UCUC clear_io(PORTD, 5)
#define tgl_UCUC toggle_io(PORTD, 5)
#define get_UCUC read_io(PORTD, 5)


/************************************************************************/
/* Registers' structure                                                 */
/************************************************************************/
typedef struct
{
	uint8_t REG_THRESHOLDS;
	int16_t REG_DATA[2];
	uint8_t REG_INPUTS;
	uint8_t REG_RESERVED0;
	uint8_t REG_RESERVED1;
	uint16_t REG_FEEDBACK;
	uint8_t REG_RESERVED2;
	uint8_t REG_RESET_ANGLE;
	uint8_t REG_RESET_MOTOR;
	uint8_t REG_HIDE_LEVER;
	uint8_t REG_SET_DOUTS;
	uint8_t REG_CLEAR_DOUTS;
	uint8_t REG_EN_LED_CONFS;
	uint8_t REG_DIS_LED_CONFS;
	uint8_t REG_LEDS[9];
	uint8_t REG_DOUT4_PULSE;
	uint8_t REG_DOUT5_PULSE;
	uint8_t REG_RESERVED3;
	uint8_t REG_SAMPLE_RATE;
	uint8_t REG_USE_FEEDBACK;
	uint8_t REG_MOTOR_MICROSTEP;
	uint8_t REG_RESERVED4;
	uint8_t REG_RESERVED5;
	uint16_t REG_POS_CURRENT;
	uint16_t REG_POS_TARGET;
	uint16_t REG_POS_0;
	uint16_t REG_POS_1;
	uint16_t REG_POS_2;
	uint16_t REG_POS_MAXIMUM;
	uint8_t REG_RESERVED7;
	uint8_t REG_LED_CONF0_WHICH;
	uint8_t REG_LED_CONF0_GREEN;
	uint8_t REG_LED_CONF0_RED;
	uint8_t REG_LED_CONF0_BLUE;
	uint8_t REG_LED_CONF1_WHICH;
	uint8_t REG_LED_CONF1_GREEN;
	uint8_t REG_LED_CONF1_RED;
	uint8_t REG_LED_CONF1_BLUE;
	uint8_t REG_LED_CONF2_WHICH;
	uint8_t REG_LED_CONF2_GREEN;
	uint8_t REG_LED_CONF2_RED;
	uint8_t REG_LED_CONF2_BLUE;
	uint8_t REG_LED_CONF3_WHICH;
	uint8_t REG_LED_CONF3_GREEN;
	uint8_t REG_LED_CONF3_RED;
	uint8_t REG_LED_CONF3_BLUE;
	uint8_t REG_LED_CONF4_WHICH;
	uint8_t REG_LED_CONF4_GREEN;
	uint8_t REG_LED_CONF4_RED;
	uint8_t REG_LED_CONF4_BLUE;
	uint8_t REG_LED_CONF5_WHICH;
	uint8_t REG_LED_CONF5_GREEN;
	uint8_t REG_LED_CONF5_RED;
	uint8_t REG_LED_CONF5_BLUE;
	uint8_t REG_LED_CONF6_WHICH;
	uint8_t REG_LED_CONF6_GREEN;
	uint8_t REG_LED_CONF6_RED;
	uint8_t REG_LED_CONF6_BLUE;
	uint8_t REG_LED_CONF7_WHICH;
	uint8_t REG_LED_CONF7_GREEN;
	uint8_t REG_LED_CONF7_RED;
	uint8_t REG_LED_CONF7_BLUE;
	uint8_t REG_RESERVED8;
	uint8_t REG_RESERVED9;
	uint16_t REG_TH_STANDBY;
	uint16_t REG_TH_0;
	uint16_t REG_TH_1;
	uint16_t REG_TH_2;
	uint16_t REG_TH_3;
	uint8_t REG_TH_STANDBY_MS;
	uint16_t REG_TH_0_ON;
	uint16_t REG_TH_0_OFF;
	uint16_t REG_TH_1_ON;
	uint16_t REG_TH_1_OFF;
	uint16_t REG_TH_2_ON;
	uint16_t REG_TH_2_OFF;
	uint16_t REG_TH_3_ON;
	uint16_t REG_TH_3_OFF;
	uint8_t REG_TH_0_LED_CONF;
	uint8_t REG_TH_1_LED_CONF;
	uint8_t REG_TH_2_LED_CONF;
	uint8_t REG_TH_3_LED_CONF;
	uint8_t REG_RESERVED10;
	uint8_t REG_RESERVED11;
	uint8_t REG_CONF_DI0;
	uint8_t REG_CONF_DI1;
	uint8_t REG_CONF_DI2;
	uint8_t REG_CONF_DI3;
	uint8_t REG_RESERVED12;
	uint8_t REG_RESERVED13;
	uint8_t REG_CONF_DO0;
	uint8_t REG_CONF_DO1;
	uint8_t REG_CONF_DO2;
	uint8_t REG_CONF_DO3;
	uint8_t REG_CONF_DO4;
	uint8_t REG_CONF_DO5;
	uint8_t REG_RESERVED14;
	uint8_t REG_RESERVED15;
	uint8_t REG_EVNT_ENABLE;
} AppRegs;

/************************************************************************/
/* Registers' address                                                   */
/************************************************************************/
/* Registers */
#define ADD_REG_THRESHOLDS                  32 // U8     State of the thresholds
#define ADD_REG_DATA                        33 // I16    Lever and ADC values
#define ADD_REG_INPUTS                      34 // U8     State of the inputs
#define ADD_REG_RESERVED0                   35 // U8     Reserved for possible future use
#define ADD_REG_RESERVED1                   36 // U8     Reserved for possible future use
#define ADD_REG_FEEDBACK                    37 // U16    Current value of the feedback circuitry
#define ADD_REG_RESERVED2                   38 // U8     Reserved for possible future use
#define ADD_REG_RESET_ANGLE                 39 // U8     Reset the rotary encoder angle to zero
#define ADD_REG_RESET_MOTOR                 40 // U8     Calibrate weight to first position and reset POS_CURRENT and POS_TARGET to ZERO
#define ADD_REG_HIDE_LEVER                  41 // U8     Controls if the lever is hidden (actuate on servo motor)
#define ADD_REG_SET_DOUTS                   42 // U8     Set digital outputs 
#define ADD_REG_CLEAR_DOUTS                 43 // U8     Clear digital outputs
#define ADD_REG_EN_LED_CONFS                44 // U8     Enable LED configurations
#define ADD_REG_DIS_LED_CONFS               45 // U8     Disable LED configurations
#define ADD_REG_LEDS                        46 // U8     [Left G] [Left R] [Left B] [Center G] [Center R] [Center B] [Right G] [Right R] [Right B]  
#define ADD_REG_DOUT4_PULSE                 47 // U8     Duration of the DOUT4 pulse in milliseconds
#define ADD_REG_DOUT5_PULSE                 48 // U8     Duration of the DOUT5 pulse in milliseconds
#define ADD_REG_RESERVED3                   49 // U8     Reserved for possible future use
#define ADD_REG_SAMPLE_RATE                 50 // U8     Lever aquiring sample rate
#define ADD_REG_USE_FEEDBACK                51 // U8     Defines if the feedback weight positiong values is used or ignored
#define ADD_REG_MOTOR_MICROSTEP             52 // U8     Defines the motor microstep
#define ADD_REG_RESERVED4                   53 // U8     Reserved for possible future use
#define ADD_REG_RESERVED5                   54 // U8     Reserved for possible future use
#define ADD_REG_POS_CURRENT                 55 // U16    Current position of the weight
#define ADD_REG_POS_TARGET                  56 // U16    Target position of the weight (the weight is converging to this value)
#define ADD_REG_POS_0                       57 // U16    Define position 0 that can be triggered using digital input 1
#define ADD_REG_POS_1                       58 // U16    Define position 0 that can be triggered using digital input 2
#define ADD_REG_POS_2                       59 // U16    Define position 0 that can be triggered using digital input 3
#define ADD_REG_POS_MAXIMUM                 60 // U16    Defines the maximum travel distance of the load
#define ADD_REG_RESERVED7                   61 // U8     Reserved for possible future use
#define ADD_REG_LED_CONF0_WHICH             62 // U8     Which LED CONF0 configuration is applied
#define ADD_REG_LED_CONF0_GREEN             63 // U8     RED intensity on configuration 0
#define ADD_REG_LED_CONF0_RED               64 // U8     GREEN intensity on configuration 0
#define ADD_REG_LED_CONF0_BLUE              65 // U8     BLUE intensity on configuration 0
#define ADD_REG_LED_CONF1_WHICH             66 // U8     Which LED CONF1 configuration is applied
#define ADD_REG_LED_CONF1_GREEN             67 // U8     RED intensity on configuration 1
#define ADD_REG_LED_CONF1_RED               68 // U8     GREEN intensity on configuration 1
#define ADD_REG_LED_CONF1_BLUE              69 // U8     BLUE intensity on configuration 1
#define ADD_REG_LED_CONF2_WHICH             70 // U8     Which LED CONF2 configuration is applied
#define ADD_REG_LED_CONF2_GREEN             71 // U8     RED intensity on configuration 2
#define ADD_REG_LED_CONF2_RED               72 // U8     GREEN intensity on configuration 2
#define ADD_REG_LED_CONF2_BLUE              73 // U8     BLUE intensity on configuration 2
#define ADD_REG_LED_CONF3_WHICH             74 // U8     Which LED CONF3 configuration is applied
#define ADD_REG_LED_CONF3_GREEN             75 // U8     RED intensity on configuration 3
#define ADD_REG_LED_CONF3_RED               76 // U8     GREEN intensity on configuration 3
#define ADD_REG_LED_CONF3_BLUE              77 // U8     BLUE intensity on configuration 3
#define ADD_REG_LED_CONF4_WHICH             78 // U8     Which LED CONF4 configuration is applied
#define ADD_REG_LED_CONF4_GREEN             79 // U8     RED intensity on configuration 4
#define ADD_REG_LED_CONF4_RED               80 // U8     GREEN intensity on configuration 4
#define ADD_REG_LED_CONF4_BLUE              81 // U8     BLUE intensity on configuration 4
#define ADD_REG_LED_CONF5_WHICH             82 // U8     Which LED CONF5 configuration is applied
#define ADD_REG_LED_CONF5_GREEN             83 // U8     RED intensity on configuration 5
#define ADD_REG_LED_CONF5_RED               84 // U8     GREEN intensity on configuration 5
#define ADD_REG_LED_CONF5_BLUE              85 // U8     BLUE intensity on configuration 5
#define ADD_REG_LED_CONF6_WHICH             86 // U8     Which LED CONF6 configuration is applied
#define ADD_REG_LED_CONF6_GREEN             87 // U8     RED intensity on configuration 6
#define ADD_REG_LED_CONF6_RED               88 // U8     GREEN intensity on configuration 6
#define ADD_REG_LED_CONF6_BLUE              89 // U8     BLUE intensity on configuration 6
#define ADD_REG_LED_CONF7_WHICH             90 // U8     Which LED CONF7 configuration is applied
#define ADD_REG_LED_CONF7_GREEN             91 // U8     RED intensity on configuration 7
#define ADD_REG_LED_CONF7_RED               92 // U8     GREEN intensity on configuration 7
#define ADD_REG_LED_CONF7_BLUE              93 // U8     BLUE intensity on configuration 7
#define ADD_REG_RESERVED8                   94 // U8     Reserved for possible future use
#define ADD_REG_RESERVED9                   95 // U8     Reserved for possible future use
#define ADD_REG_TH_STANDBY                  96 // U16    Configures lever threshold value that defines when the lever is quiet
#define ADD_REG_TH_0                        97 // U16    Configures threshold 0
#define ADD_REG_TH_1                        98 // U16    Configures threshold 1
#define ADD_REG_TH_2                        99 // U16    Configures threshold 2
#define ADD_REG_TH_3                       100 // U16    Configures threshold 3
#define ADD_REG_TH_STANDBY_MS              101 // U8     Milliseconds needed above and bellow the threshold before consider that the lever is quiet [1:200]
#define ADD_REG_TH_0_ON                    102 // U16    Milliseconds needed above TH_0 before consider the transition [1:60000]
#define ADD_REG_TH_0_OFF                   103 // U16    Milliseconds needed bellow TH_0 before consider the transition [1:60000]
#define ADD_REG_TH_1_ON                    104 // U16    Milliseconds needed above TH_1 before consider the transition [1:60000]
#define ADD_REG_TH_1_OFF                   105 // U16    Milliseconds needed bellow TH_1 before consider the transition [1:60000]
#define ADD_REG_TH_2_ON                    106 // U16    Milliseconds needed above TH_2 before consider the transition [1:60000]
#define ADD_REG_TH_2_OFF                   107 // U16    Milliseconds needed bellow TH_2 before consider the transition [1:60000]
#define ADD_REG_TH_3_ON                    108 // U16    Milliseconds needed above TH_3 before consider the transition [1:60000]
#define ADD_REG_TH_3_OFF                   109 // U16    Milliseconds needed bellow TH_3 before consider the transition [1:60000]
#define ADD_REG_TH_0_LED_CONF              110 // U8     Select if a LED_CONF is used when the threshold 0 is crossed
#define ADD_REG_TH_1_LED_CONF              111 // U8     Select if a LED_CONF is used when the threshold 1 is crossed
#define ADD_REG_TH_2_LED_CONF              112 // U8     Select if a LED_CONF is used when the threshold 2 is crossed
#define ADD_REG_TH_3_LED_CONF              113 // U8     Select if a LED_CONF is used when the threshold 3 is crossed
#define ADD_REG_RESERVED10                 114 // U8     Reserved for possible future use
#define ADD_REG_RESERVED11                 115 // U8     Reserved for possible future use
#define ADD_REG_CONF_DI0                   116 // U8     Configure digital input 0
#define ADD_REG_CONF_DI1                   117 // U8     Configure digital input 1
#define ADD_REG_CONF_DI2                   118 // U8     Configure digital input 2
#define ADD_REG_CONF_DI3                   119 // U8     Configure digital input 3
#define ADD_REG_RESERVED12                 120 // U8     Reserved for possible future use
#define ADD_REG_RESERVED13                 121 // U8     Reserved for possible future use
#define ADD_REG_CONF_DO0                   122 // U8     Configure digital output 0
#define ADD_REG_CONF_DO1                   123 // U8     Configure digital output 1
#define ADD_REG_CONF_DO2                   124 // U8     Configure digital output 2
#define ADD_REG_CONF_DO3                   125 // U8     Configure digital output 3
#define ADD_REG_CONF_DO4                   126 // U8     Configure digital output 4
#define ADD_REG_CONF_DO5                   127 // U8     Configure digital output 5
#define ADD_REG_RESERVED14                 128 // U8     Reserved for possible future use
#define ADD_REG_RESERVED15                 129 // U8     Reserved for possible future use
#define ADD_REG_EVNT_ENABLE                130 // U8     Enable the Events

/************************************************************************/
/* PWM Generator registers' memory limits                               */
/*                                                                      */
/* DON'T change the APP_REGS_ADD_MIN value !!!                          */
/* DON'T change these names !!!                                         */
/************************************************************************/
/* Memory limits */
#define APP_REGS_ADD_MIN                    0x20
#define APP_REGS_ADD_MAX                    0x82
#define APP_NBYTES_OF_REG_BANK              130

/************************************************************************/
/* Registers' bits                                                      */
/************************************************************************/
#define B_QUIET                            (1<<0)       // Lever is quiet
#define B_TH0                              (1<<1)       // When equal to 1, threshold 0 was crossed
#define B_TH1                              (1<<2)       // When equal to 1, threshold 1 was crossed
#define B_TH2                              (1<<3)       // When equal to 1, threshold 2 was crossed
#define B_TH3                              (1<<4)       // When equal to 1, threshold 3 was crossed
#define B_IN0                              (1<<0)       // State of nput 0
#define B_IN1                              (1<<1)       // State of nput 1
#define B_IN2                              (1<<2)       // State of nput 2
#define B_IN3                              (1<<3)       // State of nput 3
#define B_RST_ANGLE                        (1<<0)       // Reset angle when write 1 to this bit
#define B_RST_MOTOR                        (1<<0)       // Reset motor when write 1 to this bit
#define B_HIDE                             (1<<0)       // Lever is hidden when equal to 1
#define B_SET_DO0                          (1<<0)       // Write one to this bit will put digital out 0 into High state
#define B_SET_DO1                          (1<<1)       // Write one to this bit will put digital out 1 into High state
#define B_SET_DO2                          (1<<2)       // Write one to this bit will put digital out 2 into High state
#define B_SET_DO3                          (1<<3)       // Write one to this bit will put digital out 3 into High state
#define B_SET_DO4                          (1<<4)       // Write one to this bit will put digital out 4 into High state
#define B_SET_DO5                          (1<<5)       // 
#define B_CLR_DO0                          (1<<0)       // 
#define B_CLR_DO1                          (1<<1)       // 
#define B_CLR_DO2                          (1<<2)       // 
#define B_CLR_DO3                          (1<<3)       // 
#define B_CLR_DO4                          (1<<4)       // 
#define B_CLR_DO5                          (1<<5)       // 
#define B_EN_LED_CONF0                     (1<<0)       // Write one to this bit will execute LED configuration 0
#define B_EN_LED_CONF1                     (1<<1)       // Write one to this bit will execute LED configuration 1
#define B_EN_LED_CONF2                     (1<<2)       // Write one to this bit will execute LED configuration 2
#define B_EN_LED_CONF3                     (1<<3)       // Write one to this bit will execute LED configuration 3
#define B_EN_LED_CONF4                     (1<<4)       // Write one to this bit will execute LED configuration 4
#define B_EN_LED_CONF5                     (1<<5)       // Write one to this bit will execute LED configuration 5
#define B_EN_LED_CONF6                     (1<<6)       // Write one to this bit will execute LED configuration 6
#define B_EN_LED_CONF7                     (1<<7)       // Write one to this bit will execute LED configuration 7
#define B_DIS_LED_CONF0                    (1<<0)       // 
#define B_DIS_LED_CONF1                    (1<<1)       // 
#define B_DIS_LED_CONF2                    (1<<2)       // 
#define B_DIS_LED_CONF3                    (1<<3)       // 
#define B_DIS_LED_CONF4                    (1<<4)       // 
#define B_DIS_LED_CONF5                    (1<<5)       // 
#define B_DIS_LED_CONF6                    (1<<6)       // 
#define B_DIS_LED_CONF7                    (1<<7)       // 
#define MSK_ACQ_RATE                       (15<<0)      // 
#define GM_ACQ_1000HZ                      (1<<0)       // 1000 Hz
#define GM_ACQ_500HZ                       (3<<0)       // 500 Hz
#define GM_ACQ_250HZ                       (7<<0)       // 250 Hz
#define GM_ACQ_125HZ                       (15<<0)      // 125 Hz
#define B_FEEDBACK                         (1<<0)       // 0 - Not used, 1 - Used
#define MSK_MICROSTEP                      (3<<0)       // 
#define GM_STEP_FULL                       (0<<0)       // Full step (2 phase)
#define GM_STEP_HALF                       (1<<0)       // Half step
#define GM_STEP_QUARTER                    (2<<0)       // Quarter step
#define GM_STEP_EIGHTH                     (3<<0)       // Eighth step
#define MSK_LED_CONF                       (3<<0)       // 
#define GM_LED_CONF_LEFT                   (0<<0)       // Left LED
#define GM_LED_CONF_CENTER                 (1<<0)       // Center LED
#define GM_LED_CONF_RIGHT                  (2<<0)       // Right LED
#define MSK_QUIET_TH                       (0x7FFF)     // Lever threshold for quiet
#define B_TX_WHEN_NOT_QUIET                (1<<15)      // Send lever position only when is not quiet
#define MSK_TH_LED_CONF                    (15<<0)      // 
#define GM_TH_LED_CONF_NOT_USED            (0<<0)       // 
#define GM_TH_LED_CONF0                    (1<<0)       // 
#define GM_TH_LED_CONF1                    (2<<0)       // 
#define GM_TH_LED_CONF2                    (3<<0)       // 
#define GM_TH_LED_CONF3                    (4<<0)       // 
#define GM_TH_LED_CONF4                    (5<<0)       // 
#define GM_TH_LED_CONF5                    (6<<0)       // 
#define GM_TH_LED_CONF6                    (7<<0)       // 
#define GM_TH_LED_CONF7                    (8<<0)       // 
#define B_CLR_TH0_LED_CONF                 (1<<6)       // Clear the LED affected by the TH_0_LED_CONF
#define B_CLR_THE_OTHER_LEDS               (1<<7)       // 
#define MSK_DI0_CONF                       (7<<0)       // 
#define GM_DI0_NOT_USED                    (0<<0)       // Do nothing / disabled.  Can be used for synchronization purposes
#define GM_DI0_SET_CONF0                   (1<<0)       // Trigger LED_CONF0 when is equal to logic level 1.
#define GM_DI0_SET_CLR_CONF0               (3<<0)       // Set LED_CONF0 and turn off the LED afected by LED_CONF0.
#define GM_DI0_HIDE_LEVER                  (4<<0)       // Hide lever when DI0 is equal to logic one
#define MSK_DI1_CONF                       (7<<0)       // 
#define GM_DI1_NOT_USED                    (0<<0)       // Do nothing / disabled.  Can be used for synchronization purposes
#define GM_DI1_SET_CONF1                   (1<<0)       // Trigger LED_CONF1 when is equal to logic level 1.
#define GM_DI1_MOTOR_POSITION0             (2<<0)       // Send weight to position MOTOR_POSITION0
#define GM_DI1_SET_CLR_CONF1               (3<<0)       // Set LED_CONF1 and turn off the LED afected by LED_CONF1.
#define GM_DI1_HIDE_LEVER                  (4<<0)       // Hide lever when DI1 is equal to logic one
#define MSK_DI2_CONF                       (7<<0)       // 
#define GM_DI2_NOT_USED                    (0<<0)       // Do nothing / disabled.  Can be used for synchronization purposes
#define GM_DI2_SET_CONF2                   (1<<0)       // Trigger LED_CONF2 when is equal to logic level 1.
#define GM_DI2_MOTOR_POSITION1             (2<<0)       // Send weight to position MOTOR_POSITION1
#define GM_DI2_SET_CLR_CONF2               (3<<0)       // Set LED_CONF2 and turn off the LED afected by LED_CONF2.
#define GM_DI2_HIDE_LEVER                  (4<<0)       // Hide lever when DI2 is equal to logic one
#define GM_DI2_SET_DO4                     (5<<0)       // Sets digital out 4 if CONF_DO4 is equal to DO4_PULSE
#define MSK_DI3_CONF                       (7<<0)       // 
#define GM_DI3_NOT_USED                    (0<<0)       // Do nothing / disabled.  Can be used for synchronization purposes
#define GM_DI3_SET_CONF3                   (1<<0)       // Trigger LED_CONF3 when is equal to logic level 1.
#define GM_DI3_MOTOR_POSITION2             (2<<0)       // Send weight to position MOTOR_POSITION2
#define GM_DI3_SET_CLR_CONF3               (3<<0)       // Set LED_CONF3 and turn off the LED afected by LED_CONF3.
#define GM_DI3_HIDE_LEVER                  (4<<0)       // Hide lever when DI2 is equal to logic one
#define GM_DI3_SET_DO5                     (5<<0)       // Sets digital out 5 if CONF_DO5 is equal to DO5_PULSE
#define MSK_DO0_CONF                       (3<<0)       // 
#define GM_DO0_SOFT                        (0<<0)       // Controlled by registers SET and CLEAR
#define GM_DO0_QUIET                       (1<<0)       // Lever is quiet / not being pressed
#define GM_DO0_ERROR                       (2<<0)       // An error was detected / motor position feedback shows a different position
#define MSK_DO1_CONF                       (3<<0)       // 
#define GM_DO1_SOFT                        (0<<0)       // Controlled by registers SET and CLEAR
#define GM_DO1_TH0                         (1<<0)       // Output follows the crosses of TH0
#define GM_DO1_ERROR                       (2<<0)       // An error was detected / motor position feedback shows a different position
#define MSK_DO2_CONF                       (3<<0)       // 
#define GM_DO2_SOFT                        (0<<0)       // Controlled by registers SET and CLEAR
#define GM_DO2_TH1                         (1<<0)       // Output follows the crosses of TH1
#define GM_DO2_ERROR                       (2<<0)       // An error was detected / motor position feedback shows a different position
#define MSK_DO3_CONF                       (3<<0)       // 
#define GM_DO3_SOFT                        (0<<0)       // Controlled by registers SET and CLEAR
#define GM_DO3_TH2                         (1<<0)       // Output follows the crosses of TH2
#define GM_DO3_ERROR                       (2<<0)       // An error was detected / motor position feedback shows a different position
#define MSK_DO4_CONF                       (3<<0)       // 
#define GM_DO4_SOFT                        (0<<0)       // Controlled by registers SET and CLEAR
#define GM_DO4_TH3                         (1<<0)       // Output follows the crosses of TH3
#define GM_DO4_ERROR                       (2<<0)       // An error was detected / motor position feedback shows a different position
#define GM_DO4_PULSE                       (3<<0)       // Controlled by register SET (can be used for a valve)
#define MSK_DO5_CONF                       (3<<0)       // 
#define GM_DO5_SOFT                        (0<<0)       // Controlled by registers SET and CLEAR
#define GM_DO5_ERROR                       (2<<0)       // An error was detected / motor position feedback shows a different position
#define GM_DO5_PULSE                       (3<<0)       // Controlled by register SET (can be used for a valve)
#define B_EVT0                             (1<<0)       // Events of registers THRESHOLDS
#define B_EVT1                             (1<<1)       // Event of register DATA
#define B_EVT2                             (1<<2)       // Event of register INPUTS
#define B_EVT3                             (1<<3)       // Event of register POS_CURRENT

#endif /* _APP_REGS_H_ */