#include <avr/io.h>
#include "hwbp_core_types.h"
#include "app_ios_and_regs.h"

/************************************************************************/
/* Configure and initialize IOs                                         */
/************************************************************************/
void init_ios(void)
{	/* Configure input pins */
	io_pin2in(&PORTD, 4, PULL_IO_TRISTATE, SENSE_IO_EDGES_BOTH);         // IN0

	/* Configure input interrupts */
	io_set_int(&PORTD, INT_LEVEL_LOW, 0, (1<<4), false);                 // IN0

	/* Configure output pins */
	io_pin2out(&PORTA, 0, OUT_IO_DIGITAL, IN_EN_IO_DIS);                 // M_SLEEP
	io_pin2out(&PORTA, 2, OUT_IO_DIGITAL, IN_EN_IO_DIS);                 // M_DIR
	io_pin2out(&PORTA, 3, OUT_IO_DIGITAL, IN_EN_IO_DIS);                 // M_MS1
	io_pin2out(&PORTA, 4, OUT_IO_DIGITAL, IN_EN_IO_DIS);                 // M_RESET
	io_pin2out(&PORTA, 5, OUT_IO_DIGITAL, IN_EN_IO_DIS);                 // M_ENABLE
	io_pin2out(&PORTA, 6, OUT_IO_DIGITAL, IN_EN_IO_DIS);                 // M_MS2
	io_pin2out(&PORTB, 0, OUT_IO_DIGITAL, IN_EN_IO_DIS);                 // OUT0
	io_pin2out(&PORTC, 7, OUT_IO_DIGITAL, IN_EN_IO_DIS);                 // LEDS
	io_pin2out(&PORTD, 0, OUT_IO_DIGITAL, IN_EN_IO_DIS);                 // SERVO_PWM
	io_pin2out(&PORTD, 1, OUT_IO_DIGITAL, IN_EN_IO_DIS);                 // M_STEP
	io_pin2out(&PORTD, 5, OUT_IO_DIGITAL, IN_EN_IO_DIS);                 // UCUC

	/* Initialize output pins */
	set_M_SLEEP;
	clr_M_DIR;
	clr_M_MS1;
	clr_M_RESET;
	set_M_ENABLE;
	clr_M_MS2;
	clr_OUT0;
	clr_LEDS;
	clr_SERVO_PWM;
	clr_M_STEP;
	clr_UCUC;
}

/************************************************************************/
/* Registers' stuff                                                     */
/************************************************************************/
AppRegs app_regs;

uint8_t app_regs_type[] = {
	TYPE_U8,
	TYPE_I16,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U16,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U16,
	TYPE_U16,
	TYPE_U16,
	TYPE_U16,
	TYPE_U16,
	TYPE_U16,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U16,
	TYPE_U16,
	TYPE_U16,
	TYPE_U16,
	TYPE_U16,
	TYPE_U8,
	TYPE_U16,
	TYPE_U16,
	TYPE_U16,
	TYPE_U16,
	TYPE_U16,
	TYPE_U16,
	TYPE_U16,
	TYPE_U16,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8,
	TYPE_U8
};

uint16_t app_regs_n_elements[] = {
	1,
	2,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	9,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1,
	1
};

uint8_t *app_regs_pointer[] = {
	(uint8_t*)(&app_regs.REG_THRESHOLDS),
	(uint8_t*)(app_regs.REG_DATA),
	(uint8_t*)(&app_regs.REG_INPUTS),
	(uint8_t*)(&app_regs.REG_RESERVED0),
	(uint8_t*)(&app_regs.REG_RESERVED1),
	(uint8_t*)(&app_regs.REG_FEEDBACK),
	(uint8_t*)(&app_regs.REG_RESERVED2),
	(uint8_t*)(&app_regs.REG_RESET_ANGLE),
	(uint8_t*)(&app_regs.REG_RESET_MOTOR),
	(uint8_t*)(&app_regs.REG_HIDE_LEVER),
	(uint8_t*)(&app_regs.REG_SET_DOUTS),
	(uint8_t*)(&app_regs.REG_CLEAR_DOUTS),
	(uint8_t*)(&app_regs.REG_EN_LED_CONFS),
	(uint8_t*)(&app_regs.REG_DIS_LED_CONFS),
	(uint8_t*)(app_regs.REG_LEDS),
	(uint8_t*)(&app_regs.REG_DOUT4_PULSE),
	(uint8_t*)(&app_regs.REG_DOUT5_PULSE),
	(uint8_t*)(&app_regs.REG_RESERVED3),
	(uint8_t*)(&app_regs.REG_SAMPLE_RATE),
	(uint8_t*)(&app_regs.REG_USE_FEEDBACK),
	(uint8_t*)(&app_regs.REG_MOTOR_MICROSTEP),
	(uint8_t*)(&app_regs.REG_RESERVED4),
	(uint8_t*)(&app_regs.REG_RESERVED5),
	(uint8_t*)(&app_regs.REG_POS_CURRENT),
	(uint8_t*)(&app_regs.REG_POS_TARGET),
	(uint8_t*)(&app_regs.REG_POS_0),
	(uint8_t*)(&app_regs.REG_POS_1),
	(uint8_t*)(&app_regs.REG_POS_2),
	(uint8_t*)(&app_regs.REG_POS_MAXIMUM),
	(uint8_t*)(&app_regs.REG_RESERVED7),
	(uint8_t*)(&app_regs.REG_LED_CONF0_WHICH),
	(uint8_t*)(&app_regs.REG_LED_CONF0_GREEN),
	(uint8_t*)(&app_regs.REG_LED_CONF0_RED),
	(uint8_t*)(&app_regs.REG_LED_CONF0_BLUE),
	(uint8_t*)(&app_regs.REG_LED_CONF1_WHICH),
	(uint8_t*)(&app_regs.REG_LED_CONF1_GREEN),
	(uint8_t*)(&app_regs.REG_LED_CONF1_RED),
	(uint8_t*)(&app_regs.REG_LED_CONF1_BLUE),
	(uint8_t*)(&app_regs.REG_LED_CONF2_WHICH),
	(uint8_t*)(&app_regs.REG_LED_CONF2_GREEN),
	(uint8_t*)(&app_regs.REG_LED_CONF2_RED),
	(uint8_t*)(&app_regs.REG_LED_CONF2_BLUE),
	(uint8_t*)(&app_regs.REG_LED_CONF3_WHICH),
	(uint8_t*)(&app_regs.REG_LED_CONF3_GREEN),
	(uint8_t*)(&app_regs.REG_LED_CONF3_RED),
	(uint8_t*)(&app_regs.REG_LED_CONF3_BLUE),
	(uint8_t*)(&app_regs.REG_LED_CONF4_WHICH),
	(uint8_t*)(&app_regs.REG_LED_CONF4_GREEN),
	(uint8_t*)(&app_regs.REG_LED_CONF4_RED),
	(uint8_t*)(&app_regs.REG_LED_CONF4_BLUE),
	(uint8_t*)(&app_regs.REG_LED_CONF5_WHICH),
	(uint8_t*)(&app_regs.REG_LED_CONF5_GREEN),
	(uint8_t*)(&app_regs.REG_LED_CONF5_RED),
	(uint8_t*)(&app_regs.REG_LED_CONF5_BLUE),
	(uint8_t*)(&app_regs.REG_LED_CONF6_WHICH),
	(uint8_t*)(&app_regs.REG_LED_CONF6_GREEN),
	(uint8_t*)(&app_regs.REG_LED_CONF6_RED),
	(uint8_t*)(&app_regs.REG_LED_CONF6_BLUE),
	(uint8_t*)(&app_regs.REG_LED_CONF7_WHICH),
	(uint8_t*)(&app_regs.REG_LED_CONF7_GREEN),
	(uint8_t*)(&app_regs.REG_LED_CONF7_RED),
	(uint8_t*)(&app_regs.REG_LED_CONF7_BLUE),
	(uint8_t*)(&app_regs.REG_RESERVED8),
	(uint8_t*)(&app_regs.REG_RESERVED9),
	(uint8_t*)(&app_regs.REG_TH_STANDBY),
	(uint8_t*)(&app_regs.REG_TH_0),
	(uint8_t*)(&app_regs.REG_TH_1),
	(uint8_t*)(&app_regs.REG_TH_2),
	(uint8_t*)(&app_regs.REG_TH_3),
	(uint8_t*)(&app_regs.REG_TH_STANDBY_MS),
	(uint8_t*)(&app_regs.REG_TH_0_ON),
	(uint8_t*)(&app_regs.REG_TH_0_OFF),
	(uint8_t*)(&app_regs.REG_TH_1_ON),
	(uint8_t*)(&app_regs.REG_TH_1_OFF),
	(uint8_t*)(&app_regs.REG_TH_2_ON),
	(uint8_t*)(&app_regs.REG_TH_2_OFF),
	(uint8_t*)(&app_regs.REG_TH_3_ON),
	(uint8_t*)(&app_regs.REG_TH_3_OFF),
	(uint8_t*)(&app_regs.REG_TH_0_LED_CONF),
	(uint8_t*)(&app_regs.REG_TH_1_LED_CONF),
	(uint8_t*)(&app_regs.REG_TH_2_LED_CONF),
	(uint8_t*)(&app_regs.REG_TH_3_LED_CONF),
	(uint8_t*)(&app_regs.REG_RESERVED10),
	(uint8_t*)(&app_regs.REG_RESERVED11),
	(uint8_t*)(&app_regs.REG_CONF_DI0),
	(uint8_t*)(&app_regs.REG_CONF_DI1),
	(uint8_t*)(&app_regs.REG_CONF_DI2),
	(uint8_t*)(&app_regs.REG_CONF_DI3),
	(uint8_t*)(&app_regs.REG_RESERVED12),
	(uint8_t*)(&app_regs.REG_RESERVED13),
	(uint8_t*)(&app_regs.REG_CONF_DO0),
	(uint8_t*)(&app_regs.REG_CONF_DO1),
	(uint8_t*)(&app_regs.REG_CONF_DO2),
	(uint8_t*)(&app_regs.REG_CONF_DO3),
	(uint8_t*)(&app_regs.REG_CONF_DO4),
	(uint8_t*)(&app_regs.REG_CONF_DO5),
	(uint8_t*)(&app_regs.REG_RESERVED14),
	(uint8_t*)(&app_regs.REG_RESERVED15),
	(uint8_t*)(&app_regs.REG_EVNT_ENABLE)
};